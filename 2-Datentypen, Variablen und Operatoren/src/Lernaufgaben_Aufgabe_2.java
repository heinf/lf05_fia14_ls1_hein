
// Erstellen Sie das Programm Dual8 = Lernaufgaben_Aufgabe_2 . Das Programm Dual8 liest eine 
// achtstellige Dualzahl (ein Byte?) 
// Zeichenweise ein (00000000 - 11111111), berechnet den entsprechenden Zahlenwert und gibt diesen 
// als Dezimalwert aus (0 - 255)

public class Lernaufgaben_Aufgabe_2 {

	public static void main(String[] args) {
		int dualzahl = 11111111;
		int a, dezimalzahl, dualzahl_ende;
		// Umrechnung Dualzahl in Dezimalzahl
		dualzahl_ende = dualzahl;
		dezimalzahl = 0;
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 0);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 1);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 2);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 3);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 4);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 5);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 6);
		a = dualzahl % 10;
		dualzahl = dualzahl / 10;
		dezimalzahl += a * Math.pow(2, 7);
		System.out.printf("%s%d %s%d", "Dualzahl:", dualzahl_ende, "<-> Dezimalzahl:", dezimalzahl);
	}

}

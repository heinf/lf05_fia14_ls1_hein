import java.util.Scanner;

public class Lernaufgaben_Aufgabe_3 {

	public static void main(String[] args) {
		// Weisen Sie durch Berechnung und Ausgabe einer entsprechenden Wahrheitstabelle
		// die
		// De Morganíschen Gesetze
		// neq(a und b) = neq a oder neq b und
		// neq( a oder b) = neq a und neq b nach.
		boolean a, b, a_neq, b_neq, rechnung1, rechnung2, rechnung3;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Gebe den ersten, booeleschen Wert ein:");
		a = tastatur.nextBoolean();
		System.out.println("Gebe den zweiten, booeleschen Wert ein:");
		b = tastatur.nextBoolean();
		a_neq = !a;
		b_neq = !b;
		System.out.println("a=" + a);
		System.out.println("b=" + b);
		System.out.println("a_=" + a_neq);
		System.out.println("b_=" + b_neq);
		rechnung1 = !(a && b);
		System.out.println("(a und b)_ = " + rechnung1);
		rechnung2 = (a_neq || b_neq);
		System.out.println("a_ oder b_ = " + rechnung2);
		rechnung3 = rechnung1 == rechnung2;
		System.out.println("Erstes Morgansches Gesetz:\n(a und b)_ = a_ oder b_\ngilt? " + rechnung3);
		rechnung1 = !(a || b);
		System.out.println("(a oder b)_ = " + rechnung1);
		rechnung2 = (a_neq && b_neq);
		System.out.println("a_ und b_ = " + rechnung2);
		rechnung3 = rechnung1 == rechnung2;
		System.out.println("Zweites Morgansches Gesetz:\n(a und b)_ = a_ oder b_\ngilt? " + rechnung3);
	}

}

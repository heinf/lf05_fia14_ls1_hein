
public class Lernaufgaben_Aufgabe_4 {

	public static void main(String[] args) {
		// Aus: Ratz, Scheffler, Seese: Grundkurs Programmieren in Java, Hanser
		// Stellen Sie sich vor, Sie machen gerade Urlaubsvertretung f�r einen
		// Verpackungsingenieur bei der Firma
		// Raviolita. Dieser hat Ihnen kurz vor seiner Abreise in den Spontanurlaub noch
		// das Programm (bzw. die Klasse)
		// Raviolita hinterlassen (in diesem Fall Raviolita = Lernaufgaben_Aufgabe_4)
		final double PI = 3.1411592;
		double u, h;
		u = 20; // geeignete Testwerte einbauen
		h = 100; // geeignete Testwerte einbauen
		// Dieses Programm f�hrt Berechnungen durch, die bei der Herstellung von
		// Konservendosen aus einem Bleist�ck mit
		// L�ngen u (Umfang der Dose in Zentimetern) und
		// Breite h (H�he der Dose in Zentimetern)
		// anfallen. Dieses Programm sollen Sie nun so vervollst�ndigen, dass es
		// ausgehend von den Variablen u und h
		// und unter der Verwendung der Konstanten pi (bzw. PI = 3.1411592) die
		// folgenden Werte berechnet und ausgibt:

		// den Durchmesser des Dosenbodens d_boden=u/pi
		double d;
		d = u / PI;
		System.out.printf("%s %.2f%s\n", "Der Durchmesser des Dosenbodens betr�gt:", d, "cm");
		// die Fl�che des Dosenbodens f_boden=pi*(d_boden/2)^2
		double f;
		f = PI * ((d / 2) * (d / 2));
		System.out.printf("%s %.2f%s\n", "Die Fl�che des Dosenbodens betr�gt:", f, "cm");
		// die Mantelfl�che der Dose f_mantel=u*h
		double m;
		m = u * h;
		System.out.printf("%s %.2f%s\n", "Die Mantelfl�che der Dose betr�gt:", m, "cm");
		// die Gesamtfl�che der Dose f_gesamt= 2*f_boden+f_mantel
		double g;
		g = 2 * f + m;
		System.out.printf("%s %.2f%s\n", "Die Gesamtfl�che der Dose betr�gt:", g, "cm");
		// das Volumen der Dose v=f_boden*h
		double v;
		v = f * h;
		System.out.printf("%s %.2f%s\n", "Das Volumen der Dose betr�gt:", v, "cm");
	}

}

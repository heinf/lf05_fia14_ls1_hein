import java.util.Scanner;

public class Aufgabe_5_Urlaub {

	public static void main(String[] args) {
		int zuhause = 0;
		int i = 0;
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie den einzuwechselnden Betrag ein [EUR]: ");
		double reisebudget = tastatur.nextDouble();
		do {
			System.out.print("Geben Sie den einzuwechselnden Betrag ein [EUR]: ");
			double betrag_wechseln = tastatur.nextDouble();
			System.out.print("Geben Sie ihr Gastland ein: ");
			String land = tastatur.next();
			if (umrechnung(land, betrag_wechseln) == 0) {
				System.out.println("F�r dieses Land ist keine Umrechung bekannt!");
			}
			while (i == 0) {
				System.out.print("Sind Sie Zuhause? [ja/nein] ");
				switch (tastatur.next()) {
				case "ja":
					zuhause = 1;
					i = 1;
					break;
				case "nein":
					zuhause = 0;
					i = 1;
					break;
				default:
					System.out.println("Ung�ltige Eingabe");
					break;
				}
			}
			i = 0;
		} while (zuhause == 0);
	}

	public static double umrechnung(String land, double betrag_wechseln) {
		double umrechnungsfaktor = 0;
		String bezeichner = "";
		int i = 0;
		switch (land) {
		case "usa":
		case "USA":
			umrechnungsfaktor = 1.22;
			bezeichner = "USD";
			break;
		case "japan":
		case "Japan":
			umrechnungsfaktor = 126.50;
			bezeichner = "JPY";
			break;
		case "england":
		case "UK":
		case "England":
			umrechnungsfaktor = 0.89;
			bezeichner = "GBP";
			break;
		case "schweiz":
		case "Schweiz":
			umrechnungsfaktor = 1.08;
			bezeichner = "CHF";
			break;
		case "schweden":
		case "Schweden":
			umrechnungsfaktor = 10.10;
			bezeichner = "SEK";
			break;
		default:
			i = 1;
			break;
		}
		if (i == 0) {
			System.out.println(
					betrag_wechseln + " EUR" + " => " + betrag_wechseln * umrechnungsfaktor + " " + bezeichner);
		}
		return betrag_wechseln * umrechnungsfaktor;
	}
}

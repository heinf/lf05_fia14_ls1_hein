import java.util.Scanner;

public class Aufgabe_4_Volumenberechnung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double a, b, c, V, pi;
		pi = 3.141;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben sie die Seitenl�nge des W�rfels ein:");
		a = tastatur.nextDouble();
		V = wuerfel(a);
		System.out.println("Der W�rfel hat ein Volumen von: " + V);
		System.out.println("Geben sie die 3 Seitenl�ngen des Quaders ein:");
		a = tastatur.nextDouble();
		b = tastatur.nextDouble();
		c = tastatur.nextDouble();
		V = quader(a, b, c);
		System.out.println("Der Quader hat ein Volumen von: " + V);
		System.out.println("Geben sie die Seitenl�nge und die H�he der Pyramide ein:");
		a = tastatur.nextDouble();
		b = tastatur.nextDouble();
		V = pyramide(a, b);
		System.out.println("Die Pyramide hat ein Volumen von: " + V);
		System.out.println("Geben sie den Radius der Kugel ein:");
		a = tastatur.nextDouble();
		V = kugel(a, pi);
		System.out.println("Die Kugel hat ein Volumen von: " + V);
	}

	public static double wuerfel(double a) {
		return a * a * a;
	}

	public static double quader(double a, double b, double c) {
		return a * b * c;
	}

	public static double pyramide(double a, double h) {
		return a * a * h / 3;
	}

	public static double kugel(double r, double s) {
		return (double) (4) / 3 * (r * r * r) * s;
	}
}

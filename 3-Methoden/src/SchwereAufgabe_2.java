import java.util.Scanner;

public class SchwereAufgabe_2 {
	Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

	}

	public static String liesString(String text) {
		System.out.println("was m�chten Sie bestellen?");
		String artikel = myScanner.next();
		return artikel;
	}

	public static int liesInt(String text) {
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		return anzahl;
	}

	public static double liesDouble(String text) {
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();
		return mwst, preis;
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
	}

	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}

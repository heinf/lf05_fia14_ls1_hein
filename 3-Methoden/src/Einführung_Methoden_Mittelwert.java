
public class Einf�hrung_Methoden_Mittelwert {

	public static void main(String[] args) {
		// Zur Einf�hrung von Methoden bearbeiten Sie gemeinsam mit
		// dem Lehrer das Programm Mittelwert.java = Einf�hrung_Methoden_Mittelwert und
		// implementieren die
		// Methode berechneMittelwert().

		// (E) "Eingabe"
		// Werte f�r x und y festlegen:
		// ===========================
		double x = 2.0;
		double y = 4.0;
		double m;

		m = berechneMittelwert(x, y);
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);

	}

	public static double berechneMittelwert(double a, double b) {

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		return (a + b) / 2.0;

	}

}


import java.util.Scanner;

public class SchwereAufgabe_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		double r1, r2, r_gesamt;
		System.out.print("Geben Sie hier die Gr��e ihres ersten Widerstands ein: \n");
		r1 = tastatur.nextDouble();
		System.out.print("Geben Sie hier die Gr��e ihres zweiten Widerstands ein: \n");
		r2 = tastatur.nextDouble();
		r_gesamt = reihenschaltung(r1, r2);
		System.out.print("Der Gesamtwiderstand der Reihenschaltung betr�gt: \n" + r_gesamt);
	}

	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2;
	}
}
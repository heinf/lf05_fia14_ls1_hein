
public class Einf�hrung_Methoden_Quadrieren {

	public static void main(String[] args) {

		// Anschlie�end bearbeiten Sie das Programm Quadrieren.java. Hierbei sollen
		// sowohl der Titel als auch die Bereiche EVA als separate Methoden
		// implementiert und in der main-Methode aufgerufen werden.
		double x, ergebnis;
		x = Eingabe();
		ergebnis = Verarbeitung(x);
		Ausgabe(x, ergebnis);
	}

	public static double Eingabe() {
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		double x = 5;
		return x;
	}

	public static double Verarbeitung(double x) {
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = x * x;
		return ergebnis;
	}

	public static void Ausgabe(double x, double ergebnis) {
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}

}

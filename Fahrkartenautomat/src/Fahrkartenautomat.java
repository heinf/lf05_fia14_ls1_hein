﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {
		// Anfangsbedingungen
		int[] ticket_rohlinge = ticket_rohlinge_anfangswert();
		// Tastaureingabe ermöglichen
		Scanner tastatur = new Scanner(System.in);
		double i, geldbetrag_gezahlt, geldbetrag_nochoffen, geldbetrag_rückzahlung, geldbetrag_kasse;
		double[][] gelbetrag_kasse_münzfächer_variable = geldbetrag_kasse_münzfächer_anfang();
		do {
			geldbetrag_kasse = geldbetrag_kasse_berechnen(gelbetrag_kasse_münzfächer_variable);
			geldbetrag_nochoffen = erfassen(tastatur, gelbetrag_kasse_münzfächer_variable, ticket_rohlinge);
			geldbetrag_gezahlt = bezahlen(geldbetrag_nochoffen, tastatur, gelbetrag_kasse_münzfächer_variable);
			geldbetrag_rückzahlung = geldbetrag_gezahlt - geldbetrag_nochoffen;
			if (geldbetrag_rückzahlung <= geldbetrag_kasse) {
				System.out.println("\nFahrschein wird ausgegeben");
				warten(100);
				rückzahlung(geldbetrag_rückzahlung, gelbetrag_kasse_münzfächer_variable);
				do {
					System.out.print("\nMöchten Sie weitere Käufe tätigen [ja/nein]? ");
					switch (tastatur.next()) {
					case "ja":
						i = 1;
						System.out.println();
						break;
					case "nein":
						i = 2;
						break;
					default:
						System.out.println("\nFalsche Eingabe\n");
						i = 0;
						break;
					}
				} while (i == 0);
			} else {
				System.out.println("\nNicht genügend Geld in der Kasse\n");
				i = 1;
				warten(100);
			}

		} while (i == 1);
	}

	public static int[] ticket_rohlinge_anfangswert() {
		int[] ticket_rohlinge = { 2 };
		return ticket_rohlinge;
	}

	public static double geldbetrag_kasse_berechnen(double[][] gelbetrag_kasse_münzfächer_variable) {
		double geldbetrag_kasse = 0;
		for (int i = 0; i < gelbetrag_kasse_münzfächer_variable.length; i++) {
			geldbetrag_kasse = gelbetrag_kasse_münzfächer_variable[i][0] * gelbetrag_kasse_münzfächer_variable[i][1]
					+ geldbetrag_kasse;
		}
		return geldbetrag_kasse;
	}

	public static double[][] geldbetrag_kasse_münzfächer_anfang() {
		double[][] geldbetrag_kasse_münzfächer_anfang = new double[6][3];
		// Alle Münzenfächer im Automat
		geldbetrag_kasse_münzfächer_anfang[0][0] = 0.05;
		geldbetrag_kasse_münzfächer_anfang[1][0] = 0.10;
		geldbetrag_kasse_münzfächer_anfang[2][0] = 0.20;
		geldbetrag_kasse_münzfächer_anfang[3][0] = 0.50;
		geldbetrag_kasse_münzfächer_anfang[4][0] = 1.00;
		geldbetrag_kasse_münzfächer_anfang[5][0] = 2.00;
		// Anzahl der einzelnen Münzen
		geldbetrag_kasse_münzfächer_anfang[0][1] = 2;
		geldbetrag_kasse_münzfächer_anfang[1][1] = 2;
		geldbetrag_kasse_münzfächer_anfang[2][1] = 2;
		geldbetrag_kasse_münzfächer_anfang[3][1] = 2;
		geldbetrag_kasse_münzfächer_anfang[4][1] = 2;
		geldbetrag_kasse_münzfächer_anfang[5][1] = 2;
		return geldbetrag_kasse_münzfächer_anfang;
	}

	public static void warten(int wartezeit) {
		System.out.println();
		for (int i = 0; i < 25; i++) {
			System.out.print("=");
			try {
				Thread.sleep(wartezeit);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println();
	}

	public static double erfassen(Scanner tastatur, double[][] gelbetrag_kasse_münzfächer_variable,
			int[] ticket_rohlinge) {
		int[] tickets = new int[1];
		int ticket_anzahl, n;
		int abbruchindex_1, abbruchindex_2, abbruchindex_3, löschen, länge_array, zähler_1, zähler_2;
		double geldbetrag_nochoffen = 0;
		double[] ticketpreise, temporäre_ticketpreise;
		String[] ticketnamen, temporäre_ticketnamen;
		ticketpreise = erfassen_ticketpreise();
		ticketnamen = erfassen_ticketnamen();
		System.out.println("Fahrkartenbestellvorgang:");
		warten(100);
		do {
			System.out.println("\nWählen Sie ihre Wunschkarte für Berlin aus:");
			erfassen_auswahlmenü(ticketnamen, ticketpreise);
			System.out.printf("\n%s: %.2f€\n", "Zwischensumme", geldbetrag_nochoffen);
			n = 0;
			zähler_2 = 0;
			System.out.print("Ihre Wahl: ");
			// Initalisierung und Dekleration des 1. Abbruchindexes
			abbruchindex_1 = tastatur.nextInt();
			warten(25);
			// Bezahlvorgang
			if (abbruchindex_1 == ticketpreise.length) {
				System.out.printf("\n%s: %.2f€\n", "Zwischensumme", geldbetrag_nochoffen);
				
			}
			// Wenn ein Ticket ausgewählt wird
			else if (abbruchindex_1 >= 0 && abbruchindex_1 <= (ticketpreise.length - 1)) {
				System.out.print("Anzahl der Tickets [1-10]: ");
				ticket_anzahl = tastatur.nextInt();
				warten(25);
				if (ticket_anzahl > 0 && ticket_anzahl <= 10) {
					if (ticket_anzahl <= ticket_rohlinge[0]) {
						geldbetrag_nochoffen = geldbetrag_nochoffen + ticket_anzahl * ticketpreise[abbruchindex_1];
						ticket_rohlinge[0] -= ticket_anzahl;
					} else {
						System.out.printf("\n%s\n", "==================");
						System.out.printf("\n%s\n", "Ungültige Eingabe");
						System.out.printf("\n%s\n", "==================");
						System.out.printf("\n%s%d/%d%s\n", "Nur ", ticket_rohlinge[0], ticket_anzahl,
								" Ticketrohlinge im Automaten.");
						System.out.printf("\n%s%d%s\n", "Befüllen Sie den Automaten mit ",
								(ticket_anzahl - ticket_rohlinge[0]), " Ticketrohlingen.");
					}
				} else {
					System.out.printf("\n%5s %s %5s\n", "=====", "Ungültige Eingabe", "=====");
					System.out.println("\nWählen Sie eine Ticketanzahl von 1 bis 10.\n");
				}
			}
			// Wenn man in den Adminmodus will
			else if (abbruchindex_1 == 999) {
				System.out.println("\n===================");
				System.out.println("\nAdminmodus betreten");
				System.out.println("\n===================");
				System.out.printf("\nKassenstand: %.2f€\n",
						geldbetrag_kasse_berechnen(gelbetrag_kasse_münzfächer_variable));
				System.out.printf("%s%d\n", "Ticketrohlinge: ", ticket_rohlinge[0]);
				do {
					System.out.printf("\n%s\n", "Auswahlmöglichkeiten:");
					System.out.printf("%-40s (%d)\n", "Ticketrohlinge hinzufügen", 1);
					System.out.printf("%-40s (%d)\n", "Kasse leeren/füllen", 2);
					System.out.printf("%-40s (%d)\n", "Tickets hinzufügen/löschen", 3);
					System.out.printf("%-40s (%d)\n", "Beenden", 4);
					System.out.print("Ihre Wahl: ");
					abbruchindex_2 = tastatur.nextInt();
					warten(25);
					switch (abbruchindex_2) {
					// Ticketrohlinge-Menü
					case 1:
						System.out.printf("\n%s%d\n", "Ticketrohlinge: ", ticket_rohlinge[0]);
						do {
							abbruchindex_3 = 0;
							System.out.print("\nWieviele Ticketrohlinge möchten Sie hinzufügen? ");
							ticket_anzahl = tastatur.nextInt();
							warten(25);
							if (ticket_anzahl >= 0) {
								ticket_rohlinge[0] += ticket_anzahl;
								System.out.printf("\n%s%d\n", "Ticketrohlinge: ", ticket_rohlinge[0]);
								abbruchindex_3 = 1;
							} else {
								System.out.print("\nFalsche Eingabe");
								System.out.print("Wählen sie eine postive, ganze Zahl aus!\n");
							}
						} while (abbruchindex_3 != 1);
						abbruchindex_3 = 0;
						break;
					// Kassenmenü
					case 2:
						do {
							abbruchindex_3 = 0;
							System.out.printf("\n%s\n", "Auswahlmöglichkeiten:");
							System.out.printf("%-40s (%d)\n", "Kasse füllen", 1);
							System.out.printf("%-40s (%d)\n", "Kasse leeren", 2);
							System.out.printf("%-40s (%d)\n", "Beenden", 3);
							System.out.print("Ihre Wahl: ");
							löschen = tastatur.nextInt();
							warten(25);
							switch (löschen) {
							// Kasse füllen
							case 1:
								do {
									// Für jeden Schleifendurchgang wird der Scheifenindex wieder auf Standard 0
									// setzen
									n = 0;
									System.out.printf("\n%s\n", "Auswahlmöglichkeiten:");
									// Ausgabe der Münzfächer mit Nummerierung
									for (int j = 0; j < gelbetrag_kasse_münzfächer_variable.length; j++) {
										System.out.printf("%-5.2f€ %5.0fStück (%d)\n",
												gelbetrag_kasse_münzfächer_variable[j][0],
												gelbetrag_kasse_münzfächer_variable[j][1], j);
										n += 1;
									}
									// Beenden Auswahlmöglichkeit hinzufügen
									System.out.printf("%-17s (%d)\n", "Beenden", n);
									System.out.print("Ihre Wahl: ");
									zähler_2 = tastatur.nextInt();
									// Ein Münzfach füllen
									if (zähler_2 >= 0 && zähler_2 < n) {
										System.out.print("Geben Sie hier an, wieviele Münzen sie hinzugügen möchten: ");
										// Hier werden die Münzen hinzugegebn (!Achtung als double -> kein optimaler
										// Datentyp, besser wäre int)
										gelbetrag_kasse_münzfächer_variable[zähler_2][1] = gelbetrag_kasse_münzfächer_variable[zähler_2][1]
												+ tastatur.nextDouble();
										warten(25);
									}
									// Den Vorgang beenden
									else if (zähler_2 == n) {
										System.out.print("\nVorgang wird beendet\n");
										warten(25);
									}
									// Auswahl ist größer als 6 oder kleiner als 0
									else {
										System.out.print("\nFalsche Eingabe\n");
									}
								} while (zähler_2 != n);
								// Scheifenindex wieder auf Standard 0 setzen
								n = 0;
								System.out.printf("\nDer Kassenstand beträgt: %.2f Euro.\n",
										geldbetrag_kasse_berechnen(gelbetrag_kasse_münzfächer_variable));
								break;
							// Kasse leeren
							case 2:
								do {
									// Für jeden Schleifendurchgang wird der Scheifenindex wieder auf Standard 0
									// setzen
									n = 0;
									System.out.printf("\n%s\n", "Auswahlmöglichkeiten:");
									// Ausgabe der Münzfächer mit Nummerierung
									for (int j = 0; j < gelbetrag_kasse_münzfächer_variable.length; j++) {
										System.out.printf("%-5.2f€ %5.0fStück (%d)\n",
												gelbetrag_kasse_münzfächer_variable[j][0],
												gelbetrag_kasse_münzfächer_variable[j][1], j);
										n += 1;
									}
									// Beenden Auswahlmöglichkeit hinzufügen
									System.out.printf("%-17s (%d)\n", "Beenden", n);
									System.out.print("Ihre Wahl: ");
									zähler_1 = tastatur.nextInt();
									if (zähler_1 >= 0 && zähler_1 < n) {
										// Initialisierung und Deklaration des Indexes für die erfolgreiche Münzentnahme
										int m = 0;
										do {
											System.out.print(
													"Geben Sie hier an, wieviele Münzen sie entfernen möchten: ");
											zähler_2 = tastatur.nextInt();
											warten(25);
											// Wenn mehr Münzen aus dem Automaten geholt werden sollen als im Automaten
											// sich
											// befinden
											if (gelbetrag_kasse_münzfächer_variable[zähler_1][1] - zähler_2 < 0) {
												System.out.println(
														"\nFalsche Eingabe\n\n(Es sind weniger Münzen im Automaten als was Sie auszahlen möchten)\n");
												// Der Münzentnahmeprozess war nicht erfolgreich -> Abfrage wiederholen
												m = 0;
											}
											// Wenn die gewünschte Anzahl der Münzen im Automaten sich befindet
											else {
												gelbetrag_kasse_münzfächer_variable[zähler_1][1] = gelbetrag_kasse_münzfächer_variable[zähler_1][1]
														- zähler_2;
												// Der Münzentnahmeprozess ist erfolgreich abgeschlossen worden
												m = 1;
											}
										}
										// Solange der Münzentnahmeprozess nicht einmal durchgelaufen ist mit einer
										// richtigen Münzanzahl
										while (m == 0);
									}
									// Den Vorgang beenden
									else if (zähler_1 == n) {
										System.out.print("\nVorgang wird beendet\n");
										warten(25);
									}
									// Auswahl ist größer als 6 oder kleiner als 0
									else {
										System.out.print("\nFalsche Eingabe\n");
									}
								} while (zähler_1 != n);
								// Scheifenindex wieder auf Standard 0 setzen
								n = 0;
								System.out.printf("\nDer Kassenstand beträgt: %.2f Euro.\n",
										geldbetrag_kasse_berechnen(gelbetrag_kasse_münzfächer_variable));
								break;
							case 3:
								System.out.print("\nVorgang wird beendet\n");
								abbruchindex_3 = 1;
								break;
							default:
								System.out.print("\nFalsche Eingabe");
								break;
							}
						} while (abbruchindex_3 != 1);
						abbruchindex_3 = 0;
						break;
					// Tickets erstellen oder löschen
					case 3:
						do {
							// Für jeden Schleifendurchgang wird der Scheifenindex wieder auf Standard 0
							// setzen
							n = 0;
							System.out.printf("\n%s\n", "Auswahlmöglichkeiten:");
							System.out.printf("%-40s (%d)\n", "Tickets löschen", 1);
							System.out.printf("%-40s (%d)\n", "Tickets erstellen", 2);
							System.out.printf("%-40s (%d)\n", "Beenden", 3);
							System.out.print("Ihre Wahl: ");
							zähler_2 = tastatur.nextInt();
							warten(25);
							switch (zähler_2) {
							// Tickets löschen
							case 1:
								System.out.println();
								erfassen_auswahlmenü(ticketnamen, ticketpreise);
								System.out.printf("%-52s (%d)\n", "Beenden", ticketpreise.length + 1);
								System.out.println("\n----- Information: Sie können nicht die Nummer ("
										+ ticketpreise.length + ") löschen -----");
								do {
									abbruchindex_3 = 0;
									temporäre_ticketpreise = new double[ticketpreise.length - 1];
									temporäre_ticketnamen = new String[ticketpreise.length - 1];
									System.out.print("\nWelches Ticket möchten Sie löschen: ");
									löschen = tastatur.nextInt();
									warten(25);
									if (löschen < ticketpreise.length && löschen >= 0) {
										for (int j = 0; j < ticketpreise.length; j++) {
											if (j < löschen) {
												temporäre_ticketnamen[j] = ticketnamen[j];
												temporäre_ticketpreise[j] = ticketpreise[j];
											} else if (j > löschen) {
												temporäre_ticketnamen[(j - 1)] = ticketnamen[j];
												temporäre_ticketpreise[(j - 1)] = ticketpreise[j];
											} else {

											}
										}
										ticketpreise = temporäre_ticketpreise;
										ticketnamen = temporäre_ticketnamen;
										System.out.println("Die neuen Auswahlmöglichkeiten:");
										erfassen_auswahlmenü(ticketnamen, ticketpreise);
										System.out.printf("%-52s (%d)\n", "Beenden", ticketpreise.length + 1);
									} else if (löschen == (ticketpreise.length + 1)) {
										System.out.print("\nVorgang wird beendet\n");
										abbruchindex_3 = 1;
									} else {
										erfassen_auswahlmenü(ticketnamen, ticketpreise);
										System.out.printf("%-52s (%d)\n", "Beenden", ticketpreise.length + 1);
										System.out.print("\nFalsche Eingabe");
										System.out.print("\nGeben Sie eine gültige Eingabe ein!\n\n");
									}

								} while (abbruchindex_3 != 1);
								abbruchindex_3 = 0;
								break;
							// Tickets erstellen
							case 2:
								do {
									abbruchindex_3 = 0;
									n = 0;
									System.out.print("Wieviele Tickets möchten Sie erstellen: ");
									n = tastatur.nextInt();
									System.out.println();
									if (n < 0) {
										System.out.print("\nFalsche Eingabe");
										System.out.print("\nGeben Sie positive, gerade gültige Zahl ein!\n\n");
									} else {
										länge_array = ticketpreise.length + n;
										zähler_2 = 1;
										temporäre_ticketpreise = new double[länge_array];
										temporäre_ticketnamen = new String[länge_array];
										for (int j = 0; j < ticketpreise.length; j++) {
											temporäre_ticketpreise[j] = ticketpreise[j];
											temporäre_ticketnamen[j] = ticketnamen[j];
										}
										ticketpreise = temporäre_ticketpreise;
										ticketnamen = temporäre_ticketnamen;
										for (int j = 0; j < ticketnamen.length; j++) {
											if (ticketpreise[j] == 0) {
												System.out.print("Geben Sie den Namen der " + (zähler_2)
														+ "ten Ticketart hier ein: ");
												ticketnamen[j] = tastatur.next();
												System.out.print("Geben Sie den Preis der " + (zähler_2)
														+ "ten Ticketart hier ein: ");
												ticketpreise[j] = tastatur.nextDouble();
												zähler_2 += 1;
											} else {

											}
										}
										abbruchindex_3 = 1;
									}
								} while (abbruchindex_3 != 1);
								break;
							case 3:
								System.out.println("\nTicket-Menü beenden");
								break;
							default:
								System.out.println("\nFalsche Eingabe");
								break;
							}
						} while (zähler_2 != 3);
					case 4:
						break;
					default:
						System.out.println("\nFalsche Eingabe");
						break;
					}

				}
				// Solange der Auswahlvorgang nicht beendet werden soll
				while (abbruchindex_2 != 4);
				System.out.println("\n==================");
				System.out.println("\nAdminmodus beenden");
				System.out.println("\n==================");

			} else {
				System.out.println("\nFalsche Eingabe");

			}
		}
		// Solange der Abbruchindex nicht den Bezahlvorgang (Beenden der Schleife)
		// einleitet
		while (abbruchindex_1 != ticketpreise.length);
		return geldbetrag_nochoffen;
	}

	public static void erfassen_auswahlmenü(String[] ticketnamen, double[] ticketpreise) {
		// Jede mögliche Wahl wird hier ausgageben ud nummeriert
		for (int j = 0; j < ticketnamen.length; j++) {
			System.out.printf("%-40s [%-5.2f EUR] (%d)\n", ticketnamen[j], ticketpreise[j], j);
		}
		System.out.printf("%-52s (%d)\n", "Bezahlen", ticketpreise.length);
	}

	public static double[] erfassen_ticketpreise() {
		double[] preise = new double[10];
		preise[0] = 2.90;
		preise[1] = 3.30;
		preise[2] = 3.60;
		preise[3] = 1.90;
		preise[4] = 8.60;
		preise[5] = 9.00;
		preise[6] = 9.60;
		preise[7] = 23.50;
		preise[8] = 24.30;
		preise[9] = 24.90;
		return preise;
	}

	public static String[] erfassen_ticketnamen() {
		String[] namen = new String[10];
		namen[0] = "Einzelfahrschein Berlin AB";
		namen[1] = "Einzelfahrschein Berlin BC";
		namen[2] = "Einzelfahrschein Berlin ABC";
		namen[3] = "Kurzstrecke";
		namen[4] = "Tageskarte Berlin AB";
		namen[5] = "Tageskarte Berlin BC";
		namen[6] = "Tageskarte Berlin ABC";
		namen[7] = "Kleingruppen-Tageskarte Berlin AB";
		namen[8] = "Kleingruppen-Tageskarte Berlin BC";
		namen[9] = "Kleingruppen-Tageskarte Berlin ABC";
		return namen;
	}

	public static double bezahlen(double geldbetrag_nochoffen, Scanner tastatur,
			double[][] gelbetrag_kasse_münzfächer_variable) {
		double geldeinwurf;
		double geldbetrag_gezahlt = 0.0;
		while (geldbetrag_gezahlt < geldbetrag_nochoffen) {
			System.out.printf("%s %.2f %s", "Noch zu zahlen: ", (geldbetrag_nochoffen - geldbetrag_gezahlt),
					" Euro \n");
			System.out.print("Münzeinwurf: ");
			geldeinwurf = tastatur.nextDouble();
			// Wenn 5 Cent eingeworfen werden, erhöht sich der eingezahlte Betrag um 5 Cent
			// und das 5 Cent-Münzfach wird um 1 erhöht
			if (geldeinwurf == 0.05) {
				geldbetrag_gezahlt += geldeinwurf;
				gelbetrag_kasse_münzfächer_variable[0][1] += 1;
			}
			// Wenn 10 Cent eingeworfen werden, erhöht sich der eingezahlte Betrag um 10
			// Cent
			// und das 10 Cent-Münzfach wird um 1 erhöht
			else if (geldeinwurf == 0.10) {
				geldbetrag_gezahlt += geldeinwurf;
				gelbetrag_kasse_münzfächer_variable[1][1] += 1;
			}
			// Wenn 20 Cent eingeworfen werden, erhöht sich der eingezahlte Betrag um 20
			// Cent
			// und das 20 Cent-Münzfach wird um 1 erhöht
			else if (geldeinwurf == 0.20) {
				geldbetrag_gezahlt += geldeinwurf;
				gelbetrag_kasse_münzfächer_variable[2][1] += 1;
			}
			// Wenn 50 Cent eingeworfen werden, erhöht sich der eingezahlte Betrag um 10
			// Cent
			// und das 50 Cent-Münzfach wird um 1 erhöht
			else if (geldeinwurf == 0.50) {
				geldbetrag_gezahlt += geldeinwurf;
				gelbetrag_kasse_münzfächer_variable[3][1] += 1;
			}
			// Wenn 1 Euro eingeworfen werden, erhöht sich der eingezahlte Betrag um 1
			// Euro
			// und das 1 Euro-Münzfach wird um 1 erhöht
			else if (geldeinwurf == 1.00) {
				geldbetrag_gezahlt += geldeinwurf;
				gelbetrag_kasse_münzfächer_variable[4][1] += 1;
			}
			// Wenn 2 Euro eingeworfen werden, erhöht sich der eingezahlte Betrag um 2
			// Euro
			// und das 2 Euro-Münzfach wird um 1 erhöht
			else if (geldeinwurf == 2.00) {
				geldbetrag_gezahlt += geldeinwurf;
				gelbetrag_kasse_münzfächer_variable[5][1] += 1;
			}
			// Wenn ein Betrag eigeowrfen wird, welcher entweder nicht existiert oder vom
			// System nicht angenommen wird
			else {
				System.out.println("\nFalsche Eingabe");
			}
		}
		return geldbetrag_gezahlt;

	}

	public static void rückzahlung(double geldbetrag_rückzahlung, double[][] gelbetrag_kasse_münzfächer_variable) {
		if (geldbetrag_rückzahlung > 0.0) {

			int rechenschritte = rückzahlung_anzahl_der_rechenschritte(geldbetrag_rückzahlung);
			double[] rechenschritt_array = rückzahlung_rechenschritt_array_bilden(rechenschritte,
					geldbetrag_rückzahlung);
			if (ausgabe(rechenschritt_array, gelbetrag_kasse_münzfächer_variable) == 0) {
				System.out.printf("%s %.2f %s", "\nDer Rückgabebetrag in Höhe von ", geldbetrag_rückzahlung,
						" EURO wird in folgenden Münzen ausgezahlt:\n");
				rückzahlung_ausgabe_der_münzen(rechenschritt_array);
				System.out.println(
						"\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\nWir wünschen Ihnen eine gute Fahrt.");
			} else {
				System.out.print("\nFüllen Sie die Kasse auf\n");
			}
		}

	}

	public static int ausgabe(double[] rechenschritt_array, double[][] gelbetrag_kasse_münzfächer_variable) {
		String[] geldbeträge = { "5 Cent", "10 Cent", "20 Cent", "50 Cent", "1 Euro", "2 Euro" };
		int j = 0;
		for (int i = 0; i < rechenschritt_array.length; i++) {
			if (rechenschritt_array[i] == 0.05) {
				gelbetrag_kasse_münzfächer_variable[0][2] += 1;
			} else if (rechenschritt_array[i] == 0.10) {
				gelbetrag_kasse_münzfächer_variable[1][2] += 1;
			} else if (rechenschritt_array[i] == 0.20) {
				gelbetrag_kasse_münzfächer_variable[2][2] += 1;
			} else if (rechenschritt_array[i] == 0.50) {
				gelbetrag_kasse_münzfächer_variable[3][2] += 1;
			} else if (rechenschritt_array[i] == 1.00) {
				gelbetrag_kasse_münzfächer_variable[4][2] += 1;
			} else if (rechenschritt_array[i] == 2.00) {
				gelbetrag_kasse_münzfächer_variable[5][2] += 1;
			} else {
				System.out.println("\nFehler\n");
			}
		}
		for (int i = 0; i < gelbetrag_kasse_münzfächer_variable.length; i++) {
			if (gelbetrag_kasse_münzfächer_variable[i][1] >= gelbetrag_kasse_münzfächer_variable[i][2]) {
				gelbetrag_kasse_münzfächer_variable[i][1] = gelbetrag_kasse_münzfächer_variable[i][1]
						- gelbetrag_kasse_münzfächer_variable[i][2];
				gelbetrag_kasse_münzfächer_variable[i][2] = 0;
			} else {
				System.out.printf("\n%s%.0f%s%s\n", "Es fehlen ",
						(gelbetrag_kasse_münzfächer_variable[i][2] - gelbetrag_kasse_münzfächer_variable[i][1]), "x",
						geldbeträge[i]);
				gelbetrag_kasse_münzfächer_variable[i][2] = 0;
				j += 1;
			}
		}
		return j;
	}

	public static int rückzahlung_anzahl_der_rechenschritte(double geldbetrag_rückzahlung) {
		int rechenschritte = 0;
		while (geldbetrag_rückzahlung >= 2.0) {
			geldbetrag_rückzahlung -= 2.0;
			rechenschritte += 1;
		}
		while (geldbetrag_rückzahlung >= 1.0) {
			geldbetrag_rückzahlung -= 1.0;
			rechenschritte += 1;
		}
		while (geldbetrag_rückzahlung >= 0.5) {
			geldbetrag_rückzahlung -= 0.5;
			rechenschritte += 1;
		}
		while (geldbetrag_rückzahlung >= 0.2) {
			geldbetrag_rückzahlung -= 0.2;
			rechenschritte += 1;
		}
		while (geldbetrag_rückzahlung >= 0.09999) {
			geldbetrag_rückzahlung -= 0.1;
			rechenschritte += 1;
		}
		while (geldbetrag_rückzahlung >= 0.049999) {
			geldbetrag_rückzahlung -= 0.05;
			rechenschritte += 1;
		}
		return rechenschritte;
	}

	public static double[] rückzahlung_rechenschritt_array_bilden(int rechenschritte, double geldbetrag_rückzahlung) {
		double[] array = new double[rechenschritte];
		int i = 0;
		while (i < array.length) {
			while (geldbetrag_rückzahlung >= 2.0) {
				geldbetrag_rückzahlung -= 2.0;
				array[i] = 2.0;
				i += 1;
			}
			while (geldbetrag_rückzahlung >= 1.0) {
				geldbetrag_rückzahlung -= 1.0;
				array[i] = 1.0;
				i += 1;
			}
			while (geldbetrag_rückzahlung >= 0.5) {
				geldbetrag_rückzahlung -= 0.5;
				array[i] = 0.5;
				i += 1;
			}
			while (geldbetrag_rückzahlung >= 0.2) {
				geldbetrag_rückzahlung -= 0.2;
				array[i] = 0.2;
				i += 1;
			}
			while (geldbetrag_rückzahlung >= 0.09999) {
				geldbetrag_rückzahlung -= 0.1;
				array[i] = 0.1;
				i += 1;
			}
			while (geldbetrag_rückzahlung >= 0.049999) {
				geldbetrag_rückzahlung -= 0.05;
				array[i] = 0.05;
				i += 1;
			}
		}
		return array;
	}

	public static void rückzahlung_ausgabe_der_münzen(double[] rechenschritt_array) {
		int geldbetrag_1, geldbetrag_2, geldbetrag_3;
		String bezeichnung_1, bezeichnung_2, bezeichnung_3;
		int i = 0;
		while (i < rechenschritt_array.length) {
			if (rechenschritt_array.length - i >= 3) {
				geldbetrag_1 = rückzahlung_ausgabe_der_münzen_double_in_int(rechenschritt_array[i]);
				bezeichnung_1 = rückzahlung_ausgabe_der_münzen_double_in_string(rechenschritt_array[i]);
				geldbetrag_2 = rückzahlung_ausgabe_der_münzen_double_in_int(rechenschritt_array[i + 1]);
				bezeichnung_2 = rückzahlung_ausgabe_der_münzen_double_in_string(rechenschritt_array[i + 1]);
				geldbetrag_3 = rückzahlung_ausgabe_der_münzen_double_in_int(rechenschritt_array[i + 2]);
				bezeichnung_3 = rückzahlung_ausgabe_der_münzen_double_in_string(rechenschritt_array[i + 2]);
				rückzahlung_ausgabe_der_münzen_drei_münzen_darstellen(geldbetrag_1, geldbetrag_2, geldbetrag_3,
						bezeichnung_1, bezeichnung_2, bezeichnung_3);
				i = i + 3;
			} else if (rechenschritt_array.length - i == 2) {
				geldbetrag_1 = rückzahlung_ausgabe_der_münzen_double_in_int(rechenschritt_array[i]);
				bezeichnung_1 = rückzahlung_ausgabe_der_münzen_double_in_string(rechenschritt_array[i]);
				geldbetrag_2 = rückzahlung_ausgabe_der_münzen_double_in_int(rechenschritt_array[i + 1]);
				bezeichnung_2 = rückzahlung_ausgabe_der_münzen_double_in_string(rechenschritt_array[i + 1]);
				rückzahlung_ausgabe_der_münzen_zwei_münzen_darstellen(geldbetrag_1, geldbetrag_2, bezeichnung_1,
						bezeichnung_2);
				i = i + 2;
			} else if (rechenschritt_array.length - i == 1) {
				geldbetrag_1 = rückzahlung_ausgabe_der_münzen_double_in_int(rechenschritt_array[i]);
				bezeichnung_1 = rückzahlung_ausgabe_der_münzen_double_in_string(rechenschritt_array[i]);
				rückzahlung_ausgabe_der_münzen_eine_münze_darstellen(geldbetrag_1, bezeichnung_1);
				i = i + 1;
			} else {

			}
		}
	}

	public static int rückzahlung_ausgabe_der_münzen_double_in_int(double zahl) {
		if (zahl == 2.0) {
			return 2;
		} else if (zahl == 1.0) {
			return 1;
		} else if (zahl == 0.5) {
			return 50;
		} else if (zahl == 0.2) {
			return 20;
		} else if (zahl == 0.1) {
			return 10;
		} else if (zahl == 0.05) {
			return 5;
		} else {
			return 100;
		}
	}

	public static String rückzahlung_ausgabe_der_münzen_double_in_string(double zahl) {
		if (zahl == 2.0) {
			return "Euro";
		} else if (zahl == 1.0) {
			return "Euro";
		} else if (zahl == 0.5) {
			return "Cent";
		} else if (zahl == 0.2) {
			return "Cent";
		} else if (zahl == 0.1) {
			return "Cent";
		} else if (zahl == 0.05) {
			return "Cent";
		} else {
			return "Fehler";
		}
	}

	public static void rückzahlung_ausgabe_der_münzen_eine_münze_darstellen(int geldbetrag_1, String bezeichnung_1) {
		System.out.printf("%10s\n", "* * *");
		System.out.printf("%4s%8s\n", "*", "*");
		System.out.printf("%3s%6d%4s\n", "*", geldbetrag_1, "*");
		System.out.printf("%3s%7s%3s\n", "*", bezeichnung_1, "*");
		System.out.printf("%4s%8s\n", "*", "*");
		System.out.printf("%10s\n", "* * *");
	}

	public static void rückzahlung_ausgabe_der_münzen_zwei_münzen_darstellen(int geldbetrag_1, int geldbetrag_2,
			String bezeichnung_1, String bezeichnung_2) {
		System.out.printf("%10s%13s\n", "* * *", "* * *");
		System.out.printf("%4s%8s%5s%8s\n", "*", "*", "*", "*");
		System.out.printf("%3s%6d%4s%3s%6d%4s\n", "*", geldbetrag_1, "*", "*", geldbetrag_2, "*");
		System.out.printf("%3s%7s%3s%3s%7s%3s\n", "*", bezeichnung_1, "*", "*", bezeichnung_2, "*");
		System.out.printf("%4s%8s%5s%8s\n", "*", "*", "*", "*");
		System.out.printf("%10s%13s\n", "* * *", "* * *");
	}

	public static void rückzahlung_ausgabe_der_münzen_drei_münzen_darstellen(int geldbetrag_1, int geldbetrag_2,
			int geldbetrag_3, String bezeichnung_1, String bezeichnung_2, String bezeichnung_3) {
		System.out.printf("%10s%13s%13s\n", "* * *", "* * *", "* * *");
		System.out.printf("%4s%8s%5s%8s%5s%8s\n", "*", "*", "*", "*", "*", "*");
		System.out.printf("%3s%6d%4s%3s%6d%4s%3s%6d%4s\n", "*", geldbetrag_1, "*", "*", geldbetrag_2, "*", "*",
				geldbetrag_3, "*");
		System.out.printf("%3s%7s%3s%3s%7s%3s%3s%7s%3s\n", "*", bezeichnung_1, "*", "*", bezeichnung_2, "*", "*",
				bezeichnung_3, "*");
		System.out.printf("%4s%8s%5s%8s%5s%8s\n", "*", "*", "*", "*", "*", "*");
		System.out.printf("%10s%13s%13s\n", "* * *", "* * *", "* * *");
	}

}
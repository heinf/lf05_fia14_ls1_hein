import java.util.Scanner;

public class Einfache_Aufgabe_3 {

	public static void main(String[] args) {
		// Im Programm �Palindrom� werden �ber die Tastatur 5 Zeichen eingelesen und in
		// einem
		// geeigneten Array gespeichert. Ist dies geschehen, wird der Arrayinhalt in
		// umgekehrter
		// Reihenfolge (also von hinten nach vorn) auf der Konsole ausgegeben.
		Scanner tastatur = new Scanner(System.in);
		int[] array = new int[5];
		for (int i = 0; i < array.length; i++) {
			System.out.print("Geben Sie den " + (i + 1) + "ten Wert des Arrays ein: ");
			array[i] = tastatur.nextInt();
		}
		for (int i = array.length - 1; i >= 0; i--) {
			System.out.println(i + " | " + array[i]);
		}
	}

}

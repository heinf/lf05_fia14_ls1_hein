import java.util.Scanner;

public class Aufgabe_6 {

	public static void main(String[] args) {
		// Schreiben Sie eine Methode, die zu einer n x n Matrix (vergleiche Aufgabe
		// 2.4) �berpr�ft, ob
		// sie zu sich selber transponiert ist. Hinweis: eine n x n Matrix A ist zu sich
		// selber transponiert,
		// wenn f�r jedes Paar (i,j) gilt: aij = aji .
		// (Freiwilliger Zusatz: Versuchen Sie bei Ihrer Methode so wenige Vergleiche
		// wie m�glich
		// durchzuf�hren).
		// Testen Sie Ihre Methode in einem Hauptprogramm
		System.out.print("Geben Sie die Anzahl der Spalten/Zeilen der Matrix an: ");
		Scanner tastatur = new Scanner(System.in);
		int n_matrix = tastatur.nextInt();
		int[][] matrix = matrix_eingabe(n_matrix);
		transponiert(matrix, n_matrix);
	}

	public static int[][] matrix_eingabe(int n) {
		int[][] matrix = new int[n][n];
		Scanner tastatur = new Scanner(System.in);
		for (int i = 0; i < n; i++) {
			for (int s = 0; s < n; s++) {
				System.out.print("(" + (i + 1) + "|" + (s + 1) + "): ");
				matrix[i][s] = tastatur.nextInt();
			}
		}
		return matrix;
	}

	public static void transponiert(int[][] matrix, int n) {
		int wahl = 0;
		for (int i = 0; i < n; i++) {
			for (int s = 0; s < n; s++) {
				if (matrix[i][s] == matrix[s][i]) {
					wahl = wahl;
				} else {
					wahl = wahl + 1;
				}
			}

		}
		if (wahl == 0) {
			System.out.println("Die Matrix ist transponiert.");
		} else {
			System.out.print("Die Matrix ist nicht transponiert.");
		}
	}
}


public class Einfache_Aufgabe_1 {

	public static void main(String[] args) {
		// Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der
		// L�nge 10
		// deklariert wird. Anschlie�end wird das Array mittels Schleife mit den Zahlen
		// von 0 bis 9
		// gef�llt. Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer
		// Schleife auf der
		// Konsole aus.

		int[] array = new int[10];
		System.out.println("i" + " | " + "W" + "\n-----");
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
			System.out.println(i + " | " + array[i]);
		}

	}

}


public class Aufgabe_3 {

	public static void main(String[] args) {
		// Schreiben Sie wiederum eine Methode, die die Werte eines Feldes mit
		// ganzzahligen Werten
		// umdreht (vgl. Aufgabe 2.1). Diesmal soll jedoch das Ergebnis in ein neues
		// Feld abgelegt
		// werden. Dieses Feld soll R�ckgabewert der Methode sein.
		int[] zahlen_main = new int[10];
		System.out.print("Der generierte String lautet:\n[");
		for (int i = 0; i < zahlen_main.length; i++) {
			zahlen_main[i] = (int) (Math.random() * 100);
			System.out.printf(" %d ", zahlen_main[i]);
		}
		System.out.print("]");
		zahlen_main = umdrehen(zahlen_main);
		System.out.print("\nDer umgedrehte String lautet:\n[");
		for (int i = 0; i < zahlen_main.length; i++) {
			System.out.printf(" %d ", zahlen_main[i]);
		}
		System.out.print("]");
	}

	public static int[] umdrehen(int[] zahlen_main) {
		int[] zahlen_umdrehen = new int[10];
		for (int i = 0; i < zahlen_main.length; i++) {
			zahlen_umdrehen[i] = zahlen_main[zahlen_main.length - 1 - i];
		}
		return zahlen_umdrehen;
	}
}


public class Einfache_Aufgabe_2 {

	public static void main(String[] args) {
		// Das zu schreibende Programm �UngeradeZahlen� ist �hnlich der Aufgabe 1. Sie
		// deklarieren
		// wiederum ein Array mit 10 Ganzzahlen. Danach f�llen Sie es mit den ungeraden
		// Zahlen von 1 bis 19
		// und geben den Inhalt des Arrays �ber die Konsole aus (Verwenden Sie
		// Schleifen!).

		int[] array = new int[10];
		for (int i = 0; i < array.length; i++) {
			array[i] = i * 2 + 1;
			System.out.println(i + " | " + array[i]);
		}
	}

}


public class Aufgabe_1 {

	public static void main(String[] args) {
		// Schreiben Sie in der zu erstellenden Klasse ArrayHelper die Methode
		// public static String convertArrayToString(int[] zahlen). Die Methode soll zu
		// dem im Parameter �bergebenen Array eine mit Komma getrennte Auflistung der
		// Werte als
		// String zur�ckgeben. Testen Sie Ihre Methode in der main-Methode.
		int[] zahlen = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		System.out.print(convertArrayToString(zahlen));
	}

	public static String convertArrayToString(int[] zahlen) {
		String a = "";
		for (int i = 0; i < zahlen.length; i++) {
			// Verzweigung, damit nach letzter Array Ziffer kein (,) erscheint
			if (i < zahlen.length - 1) {
				a += zahlen[i] + ",";
			} else {
				a += zahlen[i];
			}
		}
		return a;
	}

}


public class Aufgabe_2 {

	public static void main(String[] args) {
		// Schreiben Sie eine Methode, die einen Parameter erh�lt: ein Feld mit
		// ganzzahligen Werten.
		// Die Methode soll die Reihenfolge der Elemente in dem Feld umdrehen. Dabei
		// darf kein
		// zus�tzliches Feld benutzt werden.
		// Hinweis: das erste Element muss mit dem letzten ausgetauscht werden, das
		// zweite mit dem
		// vorletzten, ...
		double[] zahlen_main = new double[10];
		System.out.print("Der generierte String lautet:\n[");
		for (int i = 0; i < zahlen_main.length; i++) {
			zahlen_main[i] = Math.random()*100;
			System.out.printf(" %.0f ", zahlen_main[i]);
		}
		System.out.print("]");
		zahlen_main = umdrehen(zahlen_main);
		System.out.print("\nDer umgekehrte String lautet:\n[");
		for (int i = 0; i < zahlen_main.length; i++) {
			System.out.printf(" %.0f ", zahlen_main[i]);
		}
		System.out.print("]");
	}

	public static double[] umdrehen(double[] zahlen_umdrehen) {
		double umtausch;
		for (int i = zahlen_umdrehen.length - 1; i >= (zahlen_umdrehen.length / 2); i--) {
			umtausch = zahlen_umdrehen[i];
			zahlen_umdrehen[i] = zahlen_umdrehen[(zahlen_umdrehen.length - 1) - i];
			zahlen_umdrehen[(zahlen_umdrehen.length - 1) - i] = umtausch;
		}
		return zahlen_umdrehen;
	}
}

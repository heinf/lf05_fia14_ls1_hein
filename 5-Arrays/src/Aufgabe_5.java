import java.util.Scanner;

public class Aufgabe_5 {

	public static void main(String[] args) {
		// Schreiben Sie eine Methode zum Einlesen von Werten in eine n x m Matrix mit
		// ganzen
		// Zahlen (realisiert als ein zweidimensionales Array). Die Werte f�r n und m
		// sollen der
		// Methode als Parameter �bergeben werden. Die eingelesene n x m Matrix soll
		// R�ckgabewert
		// der Methode sein. Testen Sie Ihre Methode in einem Hauptprogramm
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie die Anzahl der Spalten der Matrix an: ");
		int n_matrix = tastatur.nextInt();
		System.out.print("Geben Sie die Anzahl der Zeilen der Matrix an: ");
		int m_matrix = tastatur.nextInt();
		int[][] matrix = matrix_methode(n_matrix, m_matrix);
		System.out.println("");
		for (int a = 0; a < n_matrix; a++) {
			if (a == n_matrix - 1) {
				System.out.printf("%s", "-------");
			} else {
				System.out.printf("%s", "------");
			}
		}
		System.out.println("");
		for (int i = 0; i < m_matrix; i++) {
			for (int s = 0; s < n_matrix; s++) {
				if (s == n_matrix - 1) {
					System.out.printf("| %-3d |", matrix[i][s]);
				} else {
					System.out.printf("| %-3d ", matrix[i][s]);
				}
			}
			System.out.println("");
			for (int a = 0; a < n_matrix; a++) {
				if (a == n_matrix - 1) {
					System.out.printf("%s", "-------");
				} else {
					System.out.printf("%s", "------");
				}
			}
			System.out.println("");
		}
	}

	public static int[][] matrix_methode(int n, int m) {
		int[][] matrix = new int[n][m];
		Scanner tastatur = new Scanner(System.in);
		for (int i = 0; i < n; i++) {
			for (int s = 0; s < m; s++) {
				System.out.print("(" + (i + 1) + "|" + (s + 1) + "): ");
				matrix[i][s] = tastatur.nextInt();
			}
		}
		return matrix;
	}
}

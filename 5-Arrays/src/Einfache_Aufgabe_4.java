import java.util.Scanner;

public class Einfache_Aufgabe_4 {

	public static void main(String[] args) {
		// Jetzt wird Lotto gespielt. In der Klasse �Lotto� gibt es ein ganzzahliges
		// Array, welches 6 Lottozahlen
		// von 1 bis 49 aufnehmen kann. Konkret sind das die Zahlen 3, 7, 12, 18, 37 und
		// 42. Tragen Sie diese im
		// Quellcode fest ein.
		// a) Geben Sie zun�chst schleifenbasiert den Inhalt des Arrays in folgender
		// Form aus:
		// [ 3 7 12 18 37 42 ]
		// b) Pr�fen Sie nun nacheinander, ob die Zahlen 12 bzw. 13 in der Lottoziehung
		// vorkommen.
		// Geben Sie nach der Pr�fung aus:
		// Die Zahl 12 ist in der Ziehung enthalten.
		// Die Zahl 13 ist nicht in der Ziehung enthalten

		Scanner tastatur = new Scanner(System.in);
		int[] lottozahlen = new int[6];
		int zahl = 12;
		int test = 0;
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.print("Geben Sie die " + (i + 1) + "te Zahl ein: ");
			lottozahlen[i] = tastatur.nextInt();
		}
		System.out.print("[");
		for (int i = 0; i < lottozahlen.length; i++) {
			System.out.print(" " + lottozahlen[i] + " ");
		}
		System.out.print("]\n");
		while (zahl <= 13) {
			test = 0;
			for (int i = 0; i < lottozahlen.length; i++) {
				if (zahl == lottozahlen[i]) {
					test = test + 1;
				} else {
					test = test + 0;
				}
			}
			switch (zahl) {
			case 12:
				if (test == 0) {
					System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
				} else {
					System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
				}
				break;
			case 13:
				if (test == 0) {
					System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
				} else {
					System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
				}
				break;
			}
			zahl++;
		}
	}

}

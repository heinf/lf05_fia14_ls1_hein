import java.util.Scanner;

public class Aufgabe_4 {

	public static void main(String[] args) {
		// Schreiben Sie eine Methode, die eine Tabelle mit Temperaturwerten in ein
		// zweidimensionales Feld ablegt. In einer Zeile der Tabelle sollen zwei Werte
		// abgelegt
		// werden: ein Wert in Fahrenheit mit dem entsprechenden umgerechneten Wert in
		// Celsius.
		// Dabei soll in der erste Wert in Fahrenheit der Wert 0.0 sein, der zweite Wert
		// in Fahrenheit
		// der Wert 10.0, ... Der Funktion soll die Anzahl der Zeilen, d.h. die Anzahl
		// der zu
		// berechnenden Wertepaare als Parameter �bergeben werden. (Umrechnung von
		// Fahrenheit
		// F nach Celsius C: C = (5/9) � (F � 32)).
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie an, wieviel Zeilen Sie ausgeben m�chten: ");
		int anzahl = tastatur.nextInt();
		double[][] tabelle_main = temperatur(anzahl);
		for (int i = 0; i < anzahl; i++) {
			System.out.printf("%-7.2f�F | %-7.2f�C\n", tabelle_main[i][0], tabelle_main[i][1]);
		}
	}

	public static double[][] temperatur(int anzahl) {
		double[][] tabelle = new double[anzahl][2];
		tabelle[0][0] = 0.0;
		tabelle[0][1] = (double) (5) / 9 * (tabelle[0][0] - 32);
		for (int i = 1; i < anzahl; i++) {
			tabelle[i][0] = tabelle[i - 1][0] + 10;
			tabelle[i][1] = (double) (5) / 9 * (tabelle[i][0] - 32);
		}
		return tabelle;
	}
}

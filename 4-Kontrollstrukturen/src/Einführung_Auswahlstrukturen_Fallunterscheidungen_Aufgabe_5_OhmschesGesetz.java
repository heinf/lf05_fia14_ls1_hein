import java.util.Scanner;

public class Einf�hrung_Auswahlstrukturen_Fallunterscheidungen_Aufgabe_5_OhmschesGesetz {

	public static void main(String[] args) {
		// Nach dem Ohmschen Gesetz berechnet sich der Widerstand eines ohmschen
		// Widerstandes
		// mit:R= U/I
		// Schreiben Sie ein Programm, in das der Benutzer zun�chst �ber die Eingabe der
		// Buchstaben R, U oder I ausw�hlen kann, welche Gr��e berechnet werden soll.
		// Gibt er einen
		// falschen Buchstaben ein, soll eine Meldung �ber die Fehleingabe erfolgen.
		// Anschlie�end soll er die Werte der fehlenden Gr��en eingeben. Am Ende gibt
		// das
		// Programm den Wert der gesuchten Gr��e mit der richtigen Einheit aus
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie das zu berechnende Formelzeichen an [U|I|R]: ");
		String zeichen = tastatur.next();
		double I, U, R;
		switch (zeichen) {
		case "R":
			System.out.print("I[A] = ");
			I = tastatur.nextDouble();
			System.out.print("U[V] = ");
			U = tastatur.nextDouble();
			R = U / I;
			System.out.print("R = " + U + "V / " + I + "A = " + R + "Ohm");
			break;
		case "U":
			System.out.print("I[A] = ");
			I = tastatur.nextDouble();
			System.out.print("R[Ohm] = ");
			R = tastatur.nextDouble();
			U = R * I;
			System.out.print("U = " + R + "Ohm * " + I + "A = " + U + "V");
			break;
		case "I":
			System.out.print("R[Ohm] = ");
			R = tastatur.nextDouble();
			System.out.print("U[V] = ");
			U = tastatur.nextDouble();
			I = U / R;
			System.out.print("I = " + U + "V / " + R + "Ohm = " + I + "A");
			break;
		default:
			System.out.print("Falsche Eingabe!");
			break;
		}
	}

}

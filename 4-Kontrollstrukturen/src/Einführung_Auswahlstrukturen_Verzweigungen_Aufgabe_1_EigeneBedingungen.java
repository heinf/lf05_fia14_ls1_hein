import java.util.Scanner;

public class Einf�hrung_Auswahlstrukturen_Verzweigungen_Aufgabe_1_EigeneBedingungen {

	public static void main(String[] args) {
		System.out.println("Der erste Programmteil startet.");
		// Erstellen Sie folgende Programme. Es werden jeweils 2 Zahlen eingegeben:
		Scanner tastatur = new Scanner(System.in);
		double a, b, c;
		System.out.println("Geben Sie ihre erste Zahl ein:");
		a = tastatur.nextDouble();
		System.out.println("Geben Sie ihre zweite Zahl ein:");
		b = tastatur.nextDouble();
		// 1. Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag.

		// 2. Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If)
		if (a == b) {
			System.out.println("Die beiden Zahlen sind identisch.");
		} else {

		}
		// 3. Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben
		// werden (If)
		if (b > a) {
			System.out.println("Die zweite Zahl ist gr��er als die erste Zahl.");
		}
		// 4. Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung
		// ausgegeben
		// werden, ansonsten eine andere Meldung (If-Else)
		if (a >= b) {
			System.out.println("Die erste Zahl ist gr��er oder gleich der zweiten Zahl.");
		} else {
			System.out.println("Die erste Zahl ist nicht gr��er gleich der zweiten Zahl.");
		}
		System.out.println("Der zweite Programmteil startet.");
		// Erstellen Sie folgende Programme. Es werden jeweils 3 Zahlen eingegeben
		System.out.println("Geben Sie ihre erste Zahl ein:");
		a = tastatur.nextDouble();
		System.out.println("Geben Sie ihre zweite Zahl ein:");
		b = tastatur.nextDouble();
		System.out.println("Geben Sie ihre zweite dritte Zahl ein:");
		c = tastatur.nextDouble();
		// Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung
		// ausgegeben werden (If mit && / Und)
		if (a > b && a > c) {
			System.out.println("Die erste Zahl ist gr��er als die zweite und die dritte Zahl.");

		}
		// Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung
		// ausgegeben werden (If mit || / Oder)
		if (c > a || c > b) {
			System.out.println("Die dritte Zahl ist gr��er als die erste Zahl oder die zweite Zahl.");

		}
		// Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)
		if (c > a && c > b) {
			System.out.println("Die dritte Zahl ist die gr��te Zahl: " + c);

		} else if (b > a && b > c) {
			System.out.println("Die zweite Zahl ist die gr��te Zahl: " + b);
		} else {
			System.out.println("Die erste Zahl ist die gr��te Zahl: " + a);
		}
	}

}

import java.util.Scanner;

public class �bung_Schleifen_2_Aufgabe_8_Matrix {

	public static void main(String[] args) {
		// Es soll eine Multiplikationsmatrix auf dem Bildschirm (von 10 Zeilen / 10
		// Spalten) angezeigt
		// werden. Nach Eingabe einer Ziffer zwischen 2 und 9 werden alle Zahlen durch
		// einen Stern
		// gekennzeichnet, die diese Ziffer enthalten, durch die eingegebene Zahl ohne
		// Rest teilbar
		// sind oder der Quersumme entsprechen
		// Matrix
		// Bitte geben Sie eine Zahl zwischen 2 und 9 ein: 4
		// 0 1 2 3 * 5 6 7 * 9
		// 10 11 * * * 15 * 17 18 19
		// �
		// 90 91 * 93 * 95 * 97 98 99
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie ihre Zahl ein [2-9]: ");
		int zahl = tastatur.nextInt();
		int max = 99;
		int zaehler = 0;
		int durchlauf = 0;
		while (zaehler <= max) {
			while (durchlauf < 10) {
				if (quersumme(zaehler, zahl) == 1) {
					System.out.printf("%-5s", "*");
				} else if (modulo(zaehler, zahl) == 1) {
					System.out.printf("%-5s", "*");
				} else if (zifferenthalten(zaehler, zahl) == 1) {
					System.out.printf("%-5s", "*");
				} else {
					System.out.printf("%-5d", zaehler);
				}

				durchlauf++;
				zaehler++;
			}
			System.out.print("\n");
			durchlauf = 0;
		}
	}

	public static int quersumme(int zahl, int pr�fzahl) {
		int pruefsumme = 0;
		int quersumme = 0;
		while (zahl > 0) {
			pruefsumme = zahl % 10;
			zahl = zahl / 10;
			quersumme += pruefsumme;
		}
		if (quersumme == pr�fzahl) {
			return 1;
		} else {
			return 0;
		}
	}

	public static int modulo(int zahl, int pr�fzahl) {
		int pruefung;
		pruefung = zahl % pr�fzahl;
		if (pruefung == 0 && zahl != 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public static int zifferenthalten(int zahl, int pr�fzahl) {
		while (zahl > 0) {
			if (zahl % 10 == pr�fzahl) {
				return 1;
			}
			zahl = zahl / 10;
		}
		return 0;
	}
}

import java.util.Scanner;

public class Einführung_Auswahlstrukturen_Verzweigungen_Aufgabe_5_BMI {

	public static void main(String[] args) {
		// BMI (Body Mass Index): Der BMI berechnet sich aus dem Körpergewicht [kg]
		// dividiert durch
		// das Quadrat der Körpergröße [m2].
		// Die Formel lautet: BMI = (Körpergewicht in kg): (Körpergröße in m)2
		// .
		// Dies bedeutet, eine Person mit einer Körpergröße von 160 cm und einem
		// Körpergewicht von
		// 60 kg hat einen BMI von 23,4 [60: 2,56 = 23,4]. Der BMI einer Person wird
		// nach folgenden
		// Regeln klassifiziert:
		int koerpergroeße, koerpergewicht;
		double bmi;
		String geschlecht;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihre Gechlecht an: [m/w]");
		geschlecht = tastatur.next();
		System.out.println("Geben Sie ihre Körpergröße an: [cm]");
		koerpergroeße = tastatur.nextInt();
		System.out.println("Geben Sie ihr Körpergewicht an: [kg]");
		koerpergewicht = tastatur.nextInt();
		bmi = Math.round(koerpergewicht / (Math.pow((double) koerpergroeße / 100, 2)));
		if (geschlecht.contains("m")) {
			if (bmi < 20) {
				System.out.println("Sie haben Untergewicht mit einem BMI von: " + bmi);

			} else if (bmi >= 20 && bmi <= 25) {
				System.out.println("Sie haben Normalgewicht mit einem BMI von: " + bmi);
			} else if (bmi > 25) {
				System.out.println("Sie haben Übergewicht mit einem BMI von: " + bmi);
			}
		} else if (geschlecht.contains("w")) {
			if (bmi < 19) {
				System.out.println("Sie haben Untergewicht mit einem BMI von: " + bmi);

			} else if (bmi >= 19 && bmi <= 24) {
				System.out.println("Sie haben Normalgewicht mit einem BMI von: " + bmi);
			} else if (bmi > 24) {
				System.out.println("Sie haben Übergewicht mit einem BMI von: " + bmi);
			}
		} else {
			System.out.println("Falsche Eingabe!");
		}
	}

}

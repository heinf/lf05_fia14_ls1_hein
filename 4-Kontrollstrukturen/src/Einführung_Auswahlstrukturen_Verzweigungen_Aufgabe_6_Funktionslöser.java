import java.util.Scanner;

public class Einführung_Auswahlstrukturen_Verzweigungen_Aufgabe_6_Funktionslöser {

	public static void main(String[] args) {
		// Eine Funktion y = f(x) ist in folgenden Bereichen definiert:
		// x ≤ 0 f(x) = ex exponentiell
		// 0 < x ≤ 3 f(x) = x2 + 1 quadratisch
		// x > 3 f(x) = 2  x + 4 linear
		// Man lese einen Wert für x ein und gebe den Funktionswert zusammen mit der
		// Meldung aus,
		// in welchem Bereich sich der Wert befindet (konstant, linear, ...). Für die
		// Eulersche Zahl e
		// verwenden sie eine Konstante mit dem Wert 2,718.
		Scanner tastatur = new Scanner(System.in);
		double e = 2.781;
		double y;
		System.out.println("Geben Sie ihren Wert an:");
		double x = tastatur.nextDouble();
		if (x <= 0) {
			y = Math.pow(e, x);
			System.out.println("Der Funktionswert ist " + y + " und die Funktion ist im exponentiellen Bereich.");
		} else if (x > 0 && x <= 3) {
			y = Math.pow(x, 2) + 1;
			System.out.println("Der Funktionswert ist " + y + " und die Funktion ist im quadratischen Bereich.");
		} else if (x > 3) {
			y = 2 * x + 4;
			System.out.println("Der Funktionswert ist " + y + " und die Funktion ist im linearen Bereich.");
		} else {
			System.out.println("Falsche Eingabe.");
		}
	}

}

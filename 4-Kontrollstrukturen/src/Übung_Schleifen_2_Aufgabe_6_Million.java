import java.util.Scanner;

public class �bung_Schleifen_2_Aufgabe_6_Million {

	public static void main(String[] args) {
		// Entwickeln Sie ein Programm, welches die Anzahl der Jahre berechnet, bis Sie
		// aufgrund
		// einer einmaligen Einlage und einem konstanten Zinssatz Million�r geworden
		// sind. Die H�he
		// der Einlage und der Zinssatz sollen vom Benutzer eingegeben werden. Die
		// Anzahl der Jahre
		// (auf ganze Jahre aufgerundet) soll vom Programm ausgegeben werden.
		// Nach der Ausgabe soll der Benutzer gefragt werden, ob er einen weiteren
		// Programmdurchlauf mit anderen Werten durchf�hren m�chte. Best�tigt er mit
		// �j�, wird eine
		// weitere Rechnung durchgef�hrt, eine Eingabe von �n� beendet das Programm.

		Scanner tastatur = new Scanner(System.in);
		String wahl = "j";
		while (wahl.contains("j")) {
			System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
			double kapitalsumme = tastatur.nextDouble();
			System.out.print("Zinssatz: ");
			double zinssatz = tastatur.nextDouble();
			int jahr = 0;
			while (kapitalsumme < 1000000) {
				kapitalsumme = kapitalsumme + (kapitalsumme * zinssatz / 100);
				jahr++;
			}
			System.out.print("Es dauert " + jahr + " Jahre, bis Sie Million�r w�ren.");
			System.out.print("\nWiederholen? [j/n] ");
			wahl = tastatur.next();
		}
	}

}

import java.util.Scanner;

public class �bung_Schleifen_2_Aufgabe_5_Zinseszins {

	public static void main(String[] args) {
		// Es ist ein Programm zu erstellen, mit dem das Endkapital eines Sparvertrages
		// berechnet
		// werden kann. Es wird einmalig, zu Beginn des Sparvertrags, eine bestimmte
		// Kapitalsumme
		// angelegt. Der Sparvertrag l�uft �ber eine bestimmte Anzahl von Jahren, die
		// neben der
		// Anlagesumme und dem j�hrlichen Zinssatz, vom Anwender einzugeben ist. Am Ende
		// eines
		// Jahres werden die Zinsen dem jeweils vorhandenem Kapital zugeschlagen. Im
		// darauffolgenden Jahr wird das gesamte Kapital des Vorjahres (also mit den im
		// Vorjahr
		// erhaltenen Zinsen: Zinseszinseffekt!) erneut verzinst, usw.
		// Beispielhafte Bildschirmausgabe des Programms:
		// Laufzeit (in Jahren) des Sparvertrags: 20
		// Wie viel Kapital (in Euro) m�chten Sie anlegen: 10000
		// Zinssatz: 1,4
		// Eingezahltes Kapital: 10000.00 Euro
		// Ausgezahltes Kapital: 13205,63 Euro
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		int laufzeit = tastatur.nextInt();
		System.out.print("\nWie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double kapitalsumme = tastatur.nextDouble();
		System.out.print("\nZinssatz: ");
		double zinssatz = tastatur.nextDouble();
		double endsumme = kapitalsumme;
		int jahr = 0;
		while (jahr <= laufzeit) {
			endsumme = (endsumme * zinssatz / 100) + endsumme;
			jahr++;
		}
		System.out.printf("\nEingezahltes Kapital: %.0f\nAusgezahltes Kapital: %.0f", kapitalsumme, endsumme);
	}

}

import java.util.Scanner;

public class Einführung_Auswahlstrukturen_Fallunterscheidungen_Aufgabe_1_Noten {

	public static void main(String[] args) {
		// Erstellen Sie die Konsolenanwendung Noten. Das Programm Noten soll nach der
		// Eingabe
		// einer Ziffer die sprachliche Umschreibung ausgeben (1 = Sehr gut, 2 = Gut, 3
		// = Befriedigend,
		// 4 = Ausreichend, 5 = Mangelhaft, 6 = Ungenügend). Falls eine andere Ziffer
		// eingegeben
		// wird, soll ein entsprechender Fehlerhinweis ausgegeben werden.
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihre Note ein:");
		byte note = tastatur.nextByte();
		if (note == 1) {
			System.out.print("Sehr gut");
		} else if (note == 2) {
			System.out.print("Gut");
		} else if (note == 3) {
			System.out.print("Befriedigend");
		} else if (note == 4) {
			System.out.print("Ausreichend");
		} else if (note == 5) {
			System.out.print("Mangelhaft");
		} else if (note == 6) {
			System.out.print("Ungenügend");
		} else {
			System.out.print("Falsche Eingabe.");
		}
	}

}

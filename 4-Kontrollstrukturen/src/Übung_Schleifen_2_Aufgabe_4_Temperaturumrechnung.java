import java.util.Scanner;
public class �bung_Schleifen_2_Aufgabe_4_Temperaturumrechnung {

	public static void main(String[] args) {
		// Erstellen und testen Sie ein Java-Programm, das dem dargestellten Beispiel
		// entsprechend
		// Temperaturwerte von Celsius nach Fahrenheit umrechnet.
		// Bitte den Startwert in Celsius eingeben: -10
		// Bitte den Endwert in Celsius eingeben: 20
		// Bitte die Schrittweite in Grad Celsius eingeben: 5
		// -10,00�C 14,00�F
		// -5,00�C 23,00�F
		// 0,00�C 32,00�F
		// 5,00�C 41,00�F
		// 10,00�C 50,00�F
		// 15,00�C 59,00�F
		// 20,00�C 68,00�F
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		double Startwert_temperatur_C = tastatur.nextDouble();
		System.out.print("\nBitte den Endwert in Celsius eingeben: ");
		double Endwert_temperatur_C = tastatur.nextDouble();
		System.out.print("\nBitte den Schrittweite in Celsius eingeben: ");
		double Schrittweite_C = tastatur.nextDouble();
		double temperatur_F;
		System.out.printf("%-7s | %-7s\n", "T in �C", "T in �F");
		System.out.printf("%-7s | %-7s\n", "-------", "-------");
		while(Startwert_temperatur_C <= Endwert_temperatur_C) {
			temperatur_F = (Startwert_temperatur_C*1.8+32);
			System.out.printf("%-7.2f | %7.2f\n",Startwert_temperatur_C,temperatur_F);
			Startwert_temperatur_C += Schrittweite_C;
		}
	}

}

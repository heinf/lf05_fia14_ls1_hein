import java.util.Scanner;

public class �bung_Schleifen_2_Aufgabe_3_Quersumme {

	public static void main(String[] args) {
		// Berechnen Sie die Quersumme einer vorgegebenen ganzen Zahl. Die Quersumme
		// einer
		// Zahl ist die Summe aller ihrer Ziffern.
		// Tipp: Nutzen Sie den Modulo Operator.
		// Geben sie bitte eine Zahl ein: 12345
		// Die Quersumme betr�gt: 15
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Gebe die Zahl ein:");
		long zahl = tastatur.nextLong();
		long pruefsumme = 0;
		long quersumme = 0;
		System.out.print("Quersumme von " + zahl + " : ");
		while (zahl > 0) {
			if (zahl > 10) {
				pruefsumme = zahl % 10;
				zahl = zahl / 10;
				quersumme += pruefsumme;
				System.out.print(pruefsumme + " + ");
			} else {
				pruefsumme = zahl % 10;
				zahl = zahl / 10;
				quersumme += pruefsumme;
				System.out.print(pruefsumme + " = ");
			}
		}
		System.out.println(quersumme);
	}

}

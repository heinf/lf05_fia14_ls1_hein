import java.util.Scanner;

public class �bung_Schleifen_1_Aufgabe_1_Z�hlen {

	public static void main(String[] args) {
		// Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend
		// (bzw. von n bis 1
		// herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen.
		// Nutzen Sie
		// zur Umsetzung eine for-Schleife.
		// a) 1, 2, 3, �, n for-Schleife
		// b) n, �, 3, 2, 1 for-Schleife

		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihre letzte Zahl ein:");
		long x = tastatur.nextInt();
		System.out.println("M�chten Sie von oben nach unten [o], oder von unten nach oben sortieren [u]?");
		String wahl = tastatur.next();
		if (wahl.contains("o")) {
			for (long a = x; a != 0; a--) {
				System.out.print(a + " ");
			}
		} else if (wahl.contains("u")) {
			for (long a = 1; a <= x; a++) {
				System.out.print(a + " ");
			}
		} else {
			System.out.println("Falsche Eingabe.");
		}
	}

}

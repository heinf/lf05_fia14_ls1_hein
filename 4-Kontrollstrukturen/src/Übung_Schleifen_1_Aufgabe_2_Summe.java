import java.util.Scanner;

public class �bung_Schleifen_1_Aufgabe_2_Summe {

	public static void main(String[] args) {
		// Geben Sie in der Konsole die Summe der Zahlenfolgen aus. Erm�glichen Sie es
		// dem
		// Benutzer die Zahl n festzulegen, welche die Summierung begrenzt.

		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie bitte einen begrenzten Wert ein:");
		long x = tastatur.nextInt();
		long summe_a, summe_b, summe_c, b;
		summe_a = 0;
		summe_b = 0;
		summe_c = 0;
		System.out.println("Welche Scheifenart m�chten Sie benutzen? [while/for]");
		String wahl = tastatur.next();
		b = x;
		// a) 1 + 2 + 3 + 4 +�+ n for-Schleife / while-Schleife
		// b) 2 + 4 + 6 +�+ 2n for-Schleife / while-Schleife
		// c) 1 + 3 + 5 +�+ (2n+1) for-Schleife / while-Schleife
		if (wahl.contains("for")) {
			for (long a = 0; a <= x; a++) {
				summe_a += a;
			}
			for (long a = 0; a <= x; a++) {
				summe_b += 2 * a;
			}
			for (long a = 0; a <= x; a++) {
				summe_c += 2 * a + 1;
			}
			System.out.println("Die Summe f�r A betr�gt: " + summe_a);
			System.out.println("Die Summe f�r B betr�gt: " + summe_b);
			System.out.println("Die Summe f�r C betr�gt: " + summe_c);
		} else if (wahl.contains("while")) {
			while (x != -1) {
				summe_a += x;
				x--;
			}
			x = b;
			while (x != -1) {
				summe_b += 2 * x;
				x--;
			}
			x = b;
			while (x != -1) {
				summe_c = summe_c + (2 * x + 1);
				x--;
			}
			// Programmausgabe:
			// Geben Sie bitte einen begrenzenden Wert ein: 6
			// Die Summe f�r A betr�gt: 21
			// Die Summe f�r B betr�gt: 42
			// Die Summe f�r C betr�gt: 36
			System.out.println("Die Summe f�r A betr�gt: " + summe_a);
			System.out.println("Die Summe f�r B betr�gt: " + summe_b);
			System.out.println("Die Summe f�r C betr�gt: " + summe_c);
		} else {
			System.out.println("Falsche Eingabe");
		}

	}

}

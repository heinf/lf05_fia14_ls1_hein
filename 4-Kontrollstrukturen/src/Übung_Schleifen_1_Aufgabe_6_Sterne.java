import java.util.Scanner;

public class �bung_Schleifen_1_Aufgabe_6_Sterne {

	public static void main(String[] args) {
		// Schreiben Sie ein Programm, das eine von Ihnen vorgegebene Anzahl von Sternen
		// (*) in
		// Form eines Dreiecks auf dem Bildschirm ausgibt.
		// Tipp: Es existiert eine L�sung unter Nutzung einer Schleife und eine weitere
		// L�sung mit zwei
		// verschachtelten Schleifen.
		// Beispielhafte Ausgabe f�r 4 Schleifendurchl�ufe:
		// *
		// **
		// ***
		// ****
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wieviele Anzahl von Sterne m�chten Sie ausgeben lassen?");
		int sterne = tastatur.nextInt();
		int a = 1;
		int b = 1;
		while (b-1 <= sterne) {
			a = 1;
			while (a < b) {
				System.out.print("*");
				a++;
			}
			System.out.println("");
			b++;
		}

	}
}


public class �bung_Schleifen_1_Aufgabe_4_Folgen {

	public static void main(String[] args) {
		// Programmieren Sie Schleifen, welche die folgenden Folgen ausgeben:
		int b = 1;
		int c = 3;
		// a) 99, 96, 93, � 12, 9
		System.out.println("Die erste Folge a)");
		for (int a = 99; a != 6; a = a - 3) {
			System.out.print(a + " ");
		}

		// b) 1, 4, 9, 16, 25, � 361, 400
		System.out.println("\nDie zweite Folge b)");
		while (b <= 400) {
			System.out.print(b + " ");
			b = c + b;
			c += 2;
		}
		// c) 2, 6, 10, 14, � 98, 102
		System.out.println("\nDie dritte Folge c)");
		for (int a = 2; a <= 102; a = a + 4) {
			System.out.print(a + " ");
		}

		// d) 4, 16, 36, 64, 100 � 900, 1024
		b = 4;
		c = 0;
		System.out.println("\nDie vierte Folge d)");
		while (b <= 1024) {
			System.out.print(b + " ");
			b = 12 + 8 * c + b;
			c += 1;
		}
		// e) 2, 4, 8, 16, 32, �, 16384, 32768
		System.out.println("\nDie f�nfte Folge e)");
		for (int a = 2; a <= 32768; a = a * 2) {
			System.out.print(a + " ");
		}
	}

}

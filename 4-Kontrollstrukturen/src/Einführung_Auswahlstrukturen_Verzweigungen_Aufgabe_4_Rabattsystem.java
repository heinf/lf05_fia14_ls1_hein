import java.util.Scanner;

public class Einf�hrung_Auswahlstrukturen_Verzweigungen_Aufgabe_4_Rabattsystem {

	public static void main(String[] args) {
		// Der Hardware-Gro�h�ndler f�hrt ein Rabattsystem ein: Liegt der Bestellwert
		// zwischen 0 und
		// 100 �, erh�lt der Kunde einen Rabatt von 10 %. Liegt der Bestellwert h�her,
		// aber insgesamt
		// nicht �ber 500 �, betr�gt der Rabatt 15 %, in allen anderen F�llen betr�gt
		// der Rabatt 20 %.
		// Nach Eingabe des Bestellwertes soll der erm��igte Bestellwert (incl. MwSt.)
		// berechnet und
		// ausgegeben werden.
		double bestellwert, rabatt;
		final double mwst = 0.19;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihren Bestellwert ein:");
		bestellwert = tastatur.nextDouble();
		if (bestellwert <= 100) {
			rabatt = 0.1;
			bestellwert = bestellwert - bestellwert * rabatt;
			bestellwert = bestellwert + bestellwert * mwst;
		} else if (bestellwert > 100 && bestellwert < 500) {
			rabatt = 0.15;
			bestellwert = bestellwert - bestellwert * rabatt;
			bestellwert = bestellwert + bestellwert * mwst;
		} else if (bestellwert > 500) {
			rabatt = 0.2;
			bestellwert = bestellwert - bestellwert * rabatt;
			bestellwert = bestellwert + bestellwert * mwst;
		} else {
			System.out.println("Falsche Eingabe");
		}
		System.out.println("Ihr zu bezahlender Betrag betr�gt: " + bestellwert + "�");
	}
}

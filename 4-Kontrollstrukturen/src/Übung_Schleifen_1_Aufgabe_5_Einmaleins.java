
public class �bung_Schleifen_1_Aufgabe_5_Einmaleins {

	public static void main(String[] args) {
		// Sie sollen ein Programm entwickeln, welches das kleine
		// Einmaleins (1x1, 1x2 � bis 10x10) auf dem Bildschirm
		// ausgibt.
		int a = 0;
		int b = 0;
		int c = 1;
		int d = 1;
		String s = "-";
		System.out.printf("%-5s %-5d %-5d %-5d %-5d %-5d %-5d %-5d %-5d %-5d %-5d\n", "", 1, 2, 3, 4, 5, 6, 7, 8, 9,
				10);
		System.out.printf("%-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s\n", "", s, s, s, s, s, s, s, s, s, s);
		while (a < 10) {
			System.out.printf("%-5d|", c);
			while (b < 10) {
				System.out.printf("%-5d ", d);
				d = d + c;
				b++;
			}
			System.out.println("");
			b = 0;
			c = c + 1;
			d = c;
			a = a + 1;
		}
	}

}

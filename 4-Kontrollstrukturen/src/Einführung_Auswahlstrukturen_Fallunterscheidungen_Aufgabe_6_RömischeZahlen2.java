import java.util.Scanner;

public class Einf�hrung_Auswahlstrukturen_Fallunterscheidungen_Aufgabe_6_R�mischeZahlen2 {

	public static void main(String[] args) {
		// Initialisierung des Scanners "tastatur"
		Scanner tastatur = new Scanner(System.in);
		// Eingabe der R�mischen Zahl vom Benutzer
		System.out.print("Geben Sie ihre R�mische Zahl ein [I|V|X|L|C|D|M]: ");
		String zahl_string = tastatur.next();
		// Initialisierung des Arrays, welcher die R�mische Zahl speichert, der Wert 20 ist gew�hlt, damit maximal 20 Ziffern erkannt werden (Nummer ist beliebig gro� gew�hlt worden)
		int[] zahl = new int[20];
		int i = 0;
		// Initialisierung der Variable "l�nge", welche die L�nge des Arrays mit bef�llten Zahlen wiederspiegelt
		int l�nge = 0;
		int k = 0;
		String zahl_string_fest = zahl_string;
		// Wenn Wert folgende Werte enth�lt nicht ausf�hren
		int a = string_pr�fen(zahl_string);
		if (a == 0) {
			while (zahl_string.length() != 0) {
				switch (zahl_string.substring(zahl_string.length() - 1)) {
				case "I":
					zahl[i] = 1;
					break;
				case "V":
					zahl[i] = 5;
					break;
				case "X":
					zahl[i] = 10;
					break;
				case "L":
					zahl[i] = 50;
					break;
				case "C":
					zahl[i] = 100;
					break;
				case "D":
					zahl[i] = 500;
					break;
				case "M":
					zahl[i] = 1000;
					break;
				default:
					System.out.println("Falsche Eingabe");
					break;
				}
				zahl_string = zahl_string.substring(0, zahl_string.length() - 1);
				i++;
			}
			// Berechnung der L�nge des String bei weniger als 10-Zeichen
			while (zahl[k] != 0) {
				l�nge += 1;
				k++;
			}
			// �berpr�fung, ob zwei kleine Zahlen vor einer gro�en Zahl => ung�ltig
			k = 0;
			for (int j = 0; j < l�nge; j++) {
				if (zahl[j + 2] < zahl[j] && (j + 2) < l�nge) {
					k = 1;
				} else {
				}
			}
			if (k == 0) {
				if (berechnung(zahl, l�nge) == 0) {
					System.out.print("Die r�mische Zahl ist ung�ltig.");
				} else {
					System.out.println("Summe: " + zahl_string_fest + " = " + berechnung(zahl, l�nge));
				}

			} else {
				System.out.println("Sie haben vor einer gro�en Zahl mehr als 2 kleine Zahlen.");
			}
		}
	}

	public static int string_pr�fen(String zahl_string) {
		// Initaliserung der Pr�fzahl, wenn diese 0 besitzt die r�mische Zahl keine ung�ltigen H�ufung von Ziffern
		int pr�fzahl;
		if (zahl_string.contains("IIII")) {
			pr�fzahl = 1;
			System.out.println("IIII ist nicht m�glich!");
		} else if (zahl_string.contains("VV")) {
			pr�fzahl = 1;
			System.out.println("VV ist nicht m�glich!");
		} else if (zahl_string.contains("XXXX")) {
			pr�fzahl = 1;
			System.out.println("XXXX ist nicht m�glich!");
		} else if (zahl_string.contains("LL")) {
			pr�fzahl = 1;
			System.out.println("LL ist nicht m�glich!");
		} else if (zahl_string.contains("CCCC")) {
			pr�fzahl = 1;
			System.out.println("CCCC ist nicht m�glich!");
		} else if (zahl_string.contains("DD")) {
			pr�fzahl = 1;
			System.out.println("DD ist nicht m�glich!");
		} else if (zahl_string.contains("MMMM")) {
			pr�fzahl = 1;
			System.out.println("MMMM ist nicht m�glich!");
		} else {
			pr�fzahl = 0;
		}
		return pr�fzahl;
	}

	public static int berechnung(int[] zahl, int l�nge) {
		// Initalisierung der Variable des Ergebnis der Methode
		int ergebnis = 0;
		// Initalisierung des while-Schleifen Abbruchindexes
		int h = 0;
		while (h == 0) {
			// Schleife, welche den Array komplett durchl�uft
			for (int i = 0; i < l�nge; i++) {
				// Subtraktion bei kleinen Zeichen vor gro�en Zeichen
				if (zahl[i + 1] < zahl[i] && (i + 1) < l�nge) {
					// Eine 1(I) kann nur von einer 5(V) und 10(X) abgezogen werden
					if ((zahl[i] == 5 || zahl[i] == 10) && zahl[i + 1] == 1) {
						// Subtraktion der beiden Zahlen
						ergebnis = ergebnis + (zahl[i] - zahl[i + 1]);
						// Da zwei Zahlen beteiligt sind => anstelle von i++ muss 2mal i++ genommen werden
						i++;
					} 
					// Eine 10(X) kann nur von einer 50(L) und 100(C) abgezogen werden
					else if ((zahl[i] == 50 || zahl[i] == 100) && zahl[i + 1] == 10) {
						// Subtraktion der beiden Zahlen
						ergebnis = ergebnis + (zahl[i] - zahl[i + 1]);
						// Da zwei Zahlen beteiligt sind => anstelle von i++ muss 2mal i++ genommen werden
						i++;
					} 
					// Eine 100(C) kann nur von einer 500(D) und 1000(M) abgezogen werden
					else if ((zahl[i] == 500 || zahl[i] == 1000) && zahl[i + 1] == 100) {
						// Subtraktion der beiden Zahlen
						ergebnis = ergebnis + (zahl[i] - zahl[i + 1]);
						// Da zwei Zahlen beteiligt sind => anstelle von i++ muss 2mal i++ genommen werden
						i++;
					} 
					// Wenn die Subtraktionsregeln nicht angewendet werden k�nnen, obwohl in der Zahl eine kleine Ziffer vor einer gro�en Ziffer steht
					else {
						// ergebnis ist 0 und kann f�r main-Programm als Filter-Option dienen
						ergebnis = 0;
						// i = l�nge => Schleife wird abgebrochen
						i = l�nge;
					}
				}
				// Addition wenn das vordere Zeichen gr��er oder gleich dem folgenden Zeichen
				// ist
				else {
					ergebnis = ergebnis + zahl[i];
				}
			}
			// Abbruch der while-Schleife, nachdem die Berechnung abgeschlossen ist
			h = 1;
		}
		return ergebnis;
	}
}

import java.util.Scanner;

public class �bung_Schleifen_2_Aufgabe_1_Z�hlen {

	public static void main(String[] args) {
		// Geben Sie in der Konsole die nat�rlichen Zahlen von 1 bis n heraufz�hlend
		// (bzw. von n bis 1
		// herunterz�hlend) aus. Erm�glichen Sie es dem Benutzer die Zahl n festzulegen.
		// Nutzen Sie
		// zur Umsetzung eine while-Schleife.
		// a) 1, 2, 3, �, n while-Schleife
		// b) n, �, 3, 2, 1 while-Schleife
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihre maximale Zahl an:");
		int zahl = tastatur.nextInt();
		int eins = 1;
		System.out.println("Wie m�chten Sie die Zahlen ausgeben lassen? [o/u]");
		String ausgabe = tastatur.next();
		if (ausgabe.contains("o")) {
			while (zahl != 0) {
				System.out.print(zahl + " ");
				zahl -= 1;
			}
		} else if (ausgabe.contains("u")) {
			while (eins <= zahl) {
				System.out.print(eins + " ");
				eins += 1;
			}
		} else {
			System.out.println("Falsche Eingabe");
		}

	}

}

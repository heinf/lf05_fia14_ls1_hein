
public class �bung_Schleifen_1_Aufgabe_3_Module {

	public static void main(String[] args) {
		// Schreibe ein Programm, das f�r alle Zahlen zwischen 1 und 200 testet:
		for (int a = 1; a <= 200; a++) {
			System.out.print(a + " - ");
			if (a % 7 == 0) {
				System.out.println("Die Zahl ist durch 7 teilbar.");
			} else if (a % 5 >= 0 && a % 4 == 0) {
				System.out.println("Die Zahl ist durch 4 und nicht durch 5 teilbar.");
			} else {
				System.out.println("Die Zahl ist durch keine der Bedingungen teilbar.");
			}
		}
	}
}

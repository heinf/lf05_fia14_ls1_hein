import java.util.Scanner;

public class Einf�hrung_Auswahlstrukturen_Verzweigungen_Aufgabe_2_Steuersatz {

	public static void main(String[] args) {
		// Nach der Eingabe des Nettowertes soll abgefragt werden, ob der erm��igte oder
		// der volle
		// Steuersatz angewendet werden soll. Der Anwender entscheidet sich �ber die
		// Eingabe von �j�
		// f�r den erm��igten Steuersatz und mit Eingabe von �n� f�r den vollen
		// Steuersatz.
		// Anschlie�end soll das Programm den korrekten Bruttobetrag auf dem Bildschirm
		// ausgeben.
		double nettowert, bruttowert;
		boolean t = false;
		String wahl;
		final double steuersatz = 0.19;
		final double steuersatz_erm��igt = 0.07;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihren Nettowert an:");
		nettowert = tastatur.nextDouble();
		while (t != true) {
			System.out.println("Wird der erm��igte Steuersatz verwendet? [j/n]");
			wahl = tastatur.next();
			if (wahl.equals("j")) {
				bruttowert = nettowert / (1 - steuersatz_erm��igt);
				System.out.println("Der Bruttowert betr�gt: " + bruttowert + "�");
				t = true;
			} else if (wahl.equals("n")) {
				bruttowert = nettowert / (1 - steuersatz);
				System.out.println("Der Bruttowert betr�gt: " + bruttowert + "�");
				t = true;
			} else {
				System.out.println("Falsche Eingabe!");
				t = false;
			}
		}
	}

}

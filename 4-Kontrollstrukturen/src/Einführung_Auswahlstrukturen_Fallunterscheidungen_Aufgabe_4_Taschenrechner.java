import java.util.Scanner;
public class Einf�hrung_Auswahlstrukturen_Fallunterscheidungen_Aufgabe_4_Taschenrechner {

	public static void main(String[] args) {
		// Der Benutzer soll zwei Zahlen in das Programm eingeben, danach soll er
		// entscheiden, ob die
		// Zahlen addiert, subtrahiert, multipliziert oder dividiert werden, diese
		// Entscheidung soll �ber die
		// Eingabe der folgenden Symbole get�tigt werden: +, -, *, /
		// Nach der Auswahl soll das Ergebnis der Rechnung ausgegeben werden, bzw. eine
		// Fehlermeldung, falls eine falsche Auswahl getroffen wurde.
		Scanner tastatur = new Scanner(System.in);
		System.out.print("1. Zahl: ");
		int zahl1 = tastatur.nextInt();
		System.out.print("2. Zahl: ");
		int zahl2 = tastatur.nextInt();
		System.out.print("Was f�r eine Rechnung soll durchgef�hrt werden [+|-|:|*]? ");
		String wahl = tastatur.next();
		double ergebnis;
		switch(wahl) {
		case "+":
			ergebnis = zahl1+zahl2;
			System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
			break;
		case "-":
			ergebnis = zahl1-zahl2;
			System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis);
			break;
		case ":":
			ergebnis = (double)(zahl1)/zahl2;
			System.out.print(zahl1 + " : " + zahl2 + " = " + ergebnis);
			break;
		case "*":
			ergebnis = zahl1*zahl2;
			System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis);
			break;
		default:
			System.out.print("Falsche Eingabe!");
		}
	}

}

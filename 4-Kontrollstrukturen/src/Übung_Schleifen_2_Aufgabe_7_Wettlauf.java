
public class �bung_Schleifen_2_Aufgabe_7_Wettlauf {

	public static void main(String[] args) {
		// Zwei Leichtathleten f�hren einen Wettkampf �ber eine Distanz von 1000 m aus.
		// Sprinter A l�uft mit einer Geschwindigkeit von 9,5 m/s.
		// Sprinter B l�uft mit einer Geschwindigkeit von 7 m/s, da er langsamer ist,
		// erh�lt er einen
		// Vorsprung von 250 m.
		// Zeigen Sie anhand eines Programms, ob Sprinter A seinen Konkurrenten
		// �berholen kann,
		// oder ob dieser als erster ins Ziel gelangt.
		// Hinweis: Geben Sie eine Tabelle aus, die die gelaufenen Strecken der beiden
		// L�ufer f�r jede
		// volle Sekunde anzeigt. Die Berechnung soll abgebrochen werden, sobald ein
		// L�ufer das Ziel
		// erreicht.
		double sprinter_a = 9.5;
		double sprinter_b = 7;
		double strecke_a = 0;
		double strecke_b = 250;
		int sekunde = 0;
		while (strecke_a <= strecke_b) {
			System.out.printf("\n%5.0f | %-5.0f", strecke_a, strecke_b);
			strecke_a = strecke_a + sprinter_a;
			strecke_b = strecke_b + sprinter_b;
			sekunde++;
		}
		System.out.println("\nNach " + sekunde + " Sekunden �berholt der Spinter A den Sprinter B.");
	}

}

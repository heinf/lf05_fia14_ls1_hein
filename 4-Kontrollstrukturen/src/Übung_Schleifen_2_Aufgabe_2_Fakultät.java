import java.util.Scanner;

public class �bung_Schleifen_2_Aufgabe_2_Fakult�t {

	public static void main(String[] args) {
		// Schreiben Sie ein Programm, das zu einer Zahl n <= 20 die Fakult�t n!
		// ermittelt.
		// Es gilt:
		// n! = 1 * 2 * ... * (n-1) * n sowie 0! = 1
		// z.B. ist 3! = 1 * 2 * 3 = 6
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihre maximale Zahl an: [max. 20]");
		long max = tastatur.nextLong();
		long ergebnis = fakultaet(max);
		System.out.print(max + "! = ");
		while (max > 0) {
			if (max != 1) {
				System.out.print(max + " * ");
				max--;
			} else if (max == 1) {
				System.out.print(max);
				max--;
			}

		}
		System.out.print(" = " + ergebnis);
	}

	public static long fakultaet(long a) {
		long b = 1;
		while (a != 0) {
			b = b * a;
			a--;
		}
		return b;
	}

}

import java.util.Scanner;

public class Einf�hrung_Auswahlstrukturen_Verzweigungen_Aufgabe_3_Hardwaregro�h�ndler {

	public static void main(String[] args) {
		// Ein Hardware-Gro�h�ndler m�chte den Rechnungsbetrag einer Bestellung f�r
		// PC-M�use mit
		// einem Programm ermitteln. Wenn der Kunde mindestens 10 M�use bestellt,
		// erfolgt die
		// Lieferung frei Haus, bei einer geringeren Bestellmenge wird eine feste
		// Lieferpauschale von
		// 10 Euro erhoben. Vom Gro�h�ndler werden die Anzahl der bestellten M�use und
		// der
		// Einzelpreis eingegeben. Das Programm soll den Rechnungsbetrag (incl. MwSt.)
		// ausgeben.
		double rechnung, preis_maus, lieferpauschale;
		int anzahl_maus;
		final double mwst = 0.19;
		preis_maus = 10;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl der bestellten M�use ein.");
		anzahl_maus = tastatur.nextInt();
		if (anzahl_maus >= 10) {
			lieferpauschale = 0;
		} else {
			lieferpauschale = 10;
		}
		rechnung = anzahl_maus * preis_maus + (anzahl_maus * preis_maus) * mwst + lieferpauschale;
		System.out.println("Die Kosten f�r " + anzahl_maus + " M�use betr�gt " + rechnung
				+ "�, bei einem Einzelpreis von " + preis_maus + "�.");
	}
}

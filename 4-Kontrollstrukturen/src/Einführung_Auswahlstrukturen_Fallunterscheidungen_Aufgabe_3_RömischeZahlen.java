import java.util.Scanner;

public class Einführung_Auswahlstrukturen_Fallunterscheidungen_Aufgabe_3_RömischeZahlen {

	public static void main(String[] args) {
		// Erstellen Sie die Konsolenanwendung Rom. Das Programm Rom soll nach der
		// Eingabe
		// eines römischen Zahlzeichens die entsprechende Dezimalzahl ausgeben (I = 1, V
		// = 5, X =
		// 10, L = 50, C = 100, D = 500, M = 1000). Falls ein anderes Zeichen eingegeben
		// wird, soll ein
		// entsprechender Hinweis ausgegeben werden.
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie ihre Römische Zahl ein [I|V|X|L|C|D|M]: ");
		String zahl_string = tastatur.next();
		int zahl;
		switch (zahl_string) {
		case "I":
			zahl = 1;
			System.out.print(zahl_string + " = " + zahl);
			break;
		case "V":
			zahl = 5;
			System.out.print(zahl_string + " = " + zahl);
			break;
		case "X":
			zahl = 10;
			System.out.print(zahl_string + " = " + zahl);
			break;
		case "L":
			zahl = 50;
			System.out.print(zahl_string + " = " + zahl);
			break;
		case "C":
			zahl = 100;
			System.out.print(zahl_string + " = " + zahl);
			break;
		case "D":
			zahl = 500;
			System.out.print(zahl_string + " = " + zahl);
			break;
		case "M":
			zahl = 1000;
			System.out.print(zahl_string + " = " + zahl);
			break;
		default:
			System.out.print("Falsche Eingabe");
			break;
		}

	}

}
